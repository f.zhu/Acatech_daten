#!/usr/bin/env python
# coding: utf-8

import datetime
import re
import sys

import pandas as pd
import pymysql

from helpFunc import downloadFromFoerderkatalog


def databaseEstablish(cursor, database):
    sql = "CREATE DATABASE " + database  # set up the database
    sql1 = "ALTER DATABASE " + database + " character set utf8"  # chinese and japanese support
    cursor.execute(sql)
    cursor.execute(sql1)


# Path zum exportierten Datei aus Website
path_foerderkatalog_csv = "Suchliste.csv"

# Path zum definierten Katalog
path_schluesselbegriffe_csv = "data/schlüsselbegriffe.csv"

# Benutzer Info für Datenbank
database = "data_AcaTech"
user = 'root'
host = 'localhost'
password = '1234'

# Verbindung zum Datenbank herstellen
db = pymysql.connect(host, user, password)
cur = db.cursor()

cur.execute("show databases")
databaselist = [x[0] for x in cur.fetchall()]
if database in databaselist:
    cur.execute("drop schema " + database)
    db.commit()
databaseEstablish(cur, database)
cur.execute("USE " + database)


# Funktion zum Erstellen der Tabellen (Beziehung von der Tabellen in figure/Drawing111.pdf)
def tableEstablish(cursor, database, table):
    cursor.execute('USE ' + database)
    cursor.execute("SHOW TABLES")
    tablelist = [x[0] for x in cursor.fetchall()]
    if table not in tablelist:
        if table == 'Table_Förderkatalog':
            sql1 = """CREATE TABLE Table_Förderkatalog (
                     FKZ                   TEXT       NOT NULL,
                     Ressort               TEXT       NULL,
                     Zuwendungsempfänger   TEXT       NULL,
                     Stadt_Zuwendungsempfänger                 TEXT       NULL,
                     Bundesland_Zuwendungsempfänger            TEXT       NULL,
                     Thema                 TEXT       NOT NULL,
                     `Ausführende Stelle`  TEXT       NULL,
                     `stadt_Ausführende Stelle`  TEXT NULL,
                     `Bundesland_Ausführende Stelle` TEXT NULL,
                     `Laufzeit von`        DATE       NULL,
                     `Laufzeit bis`        DATE       NULL,
                     `Fördersumme in EUR`  DOUBLE     NULL,
                     Verbundprojekt        TEXT       NULL 
                     );"""
            sql2 = """ALTER TABLE Table_Förderkatalog ADD UNIQUE KEY(FKZ(20));"""  # unique constrain
            cursor.execute(sql1)
            cursor.execute(sql2)

        elif table == 'Table_Vordefinierte_Katalog':
            sql1 = """CREATE TABLE Table_Vordefinierte_Katalog (
                     Katalog          TEXT        NOT NULL
                     );"""
            sql2 = """ALTER TABLE Table_Vordefinierte_Katalog ADD UNIQUE KEY(Katalog(20));"""  # unique constrain
            cursor.execute(sql1)
            cursor.execute(sql2)

        elif table == 'Table_Katalog':
            sql1 = """CREATE TABLE Table_Katalog (
                     Katalog                   TEXT        NOT NULL,
                     übergeordnete_1           TEXT        NOT NULL,
                     übergeordnete_2           TEXT        NOT NULL,
                     übergeordnete_1_Klartext  TEXT        NOT NULL,
                     übergeordnete_2_KlarText  TEXT        NOT NULL
                     );"""
            sql2 = """ALTER TABLE Table_Katalog ADD UNIQUE KEY(Katalog(20));"""  # unique constrain
            cursor.execute(sql1)
            cursor.execute(sql2)

        elif table == 'Table_Relation_VorD_TH':
            sql1 = """CREATE TABLE Table_Relation_VorD_TH (
                     Förderkatalog_id              int         NULL,
                     Vordefinierte_Katalog_id      int         NULL
                     );"""
            sql2 = """ALTER TABLE Table_Relation_VorD_TH ADD CONSTRAINT fk_Förderkatalog_1 FOREIGN KEY (
            Förderkatalog_id) REFERENCES Table_Förderkatalog (id); """
            sql3 = """ALTER TABLE Table_Relation_VorD_TH ADD CONSTRAINT fk_Vordefinierte_Katalog FOREIGN KEY (
            Vordefinierte_Katalog_id) REFERENCES Table_Vordefinierte_Katalog (id); """
            sql4 = """ALTER TABLE Table_Relation_VorD_TH ADD UNIQUE KEY(Vordefinierte_Katalog_id,Förderkatalog_id);"""
            cursor.execute(sql1)
            cursor.execute(sql2)
            cursor.execute(sql3)
            cursor.execute(sql4)

        elif table == 'Table_Relation_KA_TH':
            sql1 = """CREATE TABLE Table_Relation_KA_TH (
                                 Förderkatalog_id              int         NULL,
                                 Katalog_id                    int         NULL
                                 );"""
            sql2 = """ALTER TABLE Table_Relation_KA_TH ADD CONSTRAINT fk_Förderkatalog_2 FOREIGN KEY (
            Förderkatalog_id) REFERENCES Table_Förderkatalog (id); """
            sql3 = """ALTER TABLE Table_Relation_KA_TH ADD CONSTRAINT fk_Katalog FOREIGN KEY (Katalog_id) REFERENCES 
            Table_Katalog (id); """
            sql4 = """ALTER TABLE Table_Relation_KA_TH ADD UNIQUE KEY(Katalog_id,Förderkatalog_id);"""
            cursor.execute(sql1)
            cursor.execute(sql2)
            cursor.execute(sql3)
            cursor.execute(sql4)

        else:
            print('undefined table')
            sys.exit()
        sql0 = """ALTER TABLE """ + table + """ ADD id INT NOT NULL PRIMARY KEY AUTO_INCREMENT;"""  # add primary key
        cursor.execute(sql0)


def createTable(cursor, database):
    tableEstablish(cursor, database, 'Table_Förderkatalog')
    tableEstablish(cursor, database, 'Table_Vordefinierte_Katalog')
    tableEstablish(cursor, database, 'Table_Katalog')
    tableEstablish(cursor, database, 'Table_Relation_VorD_TH')
    tableEstablish(cursor, database, 'Table_Relation_KA_TH')


def loadVorfilterung(path_schluesselbegriffe_csv, table, cursor):
    # Vorfilterung in Dataenbank speichern
    Vordefinierte_Katalog = pd.read_csv(path_schluesselbegriffe_csv, usecols=[0])
    Vordefinierte_Katalog.columns = ['Katalog']
    Vordefinierte_Katalog.dropna(inplace=True)
    Vordefinierte_Katalog.reset_index(drop=True, inplace=True)
    data = Vordefinierte_Katalog.to_dict('records')
    result = [(d['Katalog']) for d in data]
    for i, item in enumerate(result):
        result[i] = re.sub('_', ' ', item)
    try:
        cursor.executemany(
            """INSERT IGNORE INTO """ + table + """(Katalog) VALUES(%s)""",
            result)
    except Exception as err:
        print(err)


def loadKatalog(path_schluesselbegriffe_csv, table, cursor):
    # Katalog in Datenbank speichern
    Katalog_data = pd.read_csv(path_schluesselbegriffe_csv)
    Katalog_data.drop(['Vorfilterung'], axis=1, inplace=True)

    haupt_index = None
    haput_index_klartext = None
    for i, gruppe in enumerate(Katalog_data.columns):
        df = pd.DataFrame(columns=['Katalog', 'übergeordnete_1', 'übergeordnete_2',
                                   'übergeordnete_1_Klartext', 'übergeordnete_2_Klartext'])
        data = Katalog_data[gruppe].dropna()
        df['Katalog'] = data[1:]
        for j, k in enumerate(df['Katalog']):
            df['Katalog'].iloc[j] = re.sub('_', ' ', k)
        df['Katalog'].replace('_', ' ', inplace=True)
        index_haupt = data.name
        index_sekundaer = data[0]
        try:
            haupt_index = int(re.match(r'\d+', index_haupt, re.M | re.I).group())
            haput_index_klartext = re.sub(r'\d+\.', '', index_haupt, re.I).strip()
        except AttributeError:
            pass
        sekundaer_index = int(re.match(r'\d+\.\d+\.', index_sekundaer, re.M | re.I).group()[2])
        sekundaer_index_klartext = re.sub(r'\d+\.\d+\.', '', index_sekundaer, re.I).strip()
        df['übergeordnete_1'] = haupt_index
        df['übergeordnete_1_Klartext'] = haput_index_klartext
        df['übergeordnete_2'] = sekundaer_index
        df['übergeordnete_2_Klartext'] = sekundaer_index_klartext
        df.reset_index(drop=True, inplace=True)
        data = df.to_dict('records')
        result = [(d['Katalog'], d['übergeordnete_1'], d['übergeordnete_2'],
                   d['übergeordnete_1_Klartext'], d['übergeordnete_2_Klartext']) for d in data]
        try:
            cursor.executemany(
                """INSERT IGNORE INTO """ + table + """(Katalog, übergeordnete_1, übergeordnete_2, 
                übergeordnete_1_Klartext, übergeordnete_2_Klartext) VALUES(%s,%s,%s,%s,%s)""",
                result)
        except Exception as err:
            print(err)


def loadFdorderprojekt(path_foerderkatalog_csv, table, cursor):
    collist = ['FKZ', 'Ressort', 'Zuwendungsempfänger',
               'Stadt/Gemeinde', 'Bundesland',
               'Ausführende Stelle', 'Stadt/Gemeinde.1',
               'Bundesland.1', 'Thema', 'Laufzeit von', 'Laufzeit bis',
               'Fördersumme in EUR', 'Verbundprojekt']
    downloadFromFoerderkatalog()

    foerderkatalog = pd.read_csv(path_foerderkatalog_csv, encoding='ISO-8859-1', delimiter='";="|";|;="|="')
    foerderkatalog.drop('Unnamed: 0', axis=1, inplace=True)
    foerderkatalog.drop('Unnamed: 26', axis=1, inplace=True)
    foerderkatalog = foerderkatalog[collist]
    foerderkatalog.dropna(subset=['Laufzeit von', 'Laufzeit bis', 'Thema'], inplace=True)
    foerderkatalog.reset_index(drop=True, inplace=True)

    def datetimeTransform(x):
        date_time_obj = [datetime.datetime.strptime(y, '%d.%m.%Y') for y in x]
        date_time_obj = pd.Series(date_time_obj)
        return date_time_obj

    def intTransform(x):
        y = float(x.replace('.', '').replace(',', '.'))
        return y

    foerderkatalog[['Laufzeit von', 'Laufzeit bis']] = foerderkatalog[['Laufzeit von', 'Laufzeit bis']].apply(
        datetimeTransform, axis=0)
    foerderkatalog['Fördersumme in EUR'] = foerderkatalog['Fördersumme in EUR'].map(intTransform)
    foerderkatalog = foerderkatalog.astype(object).where(pd.notnull(foerderkatalog), None)
    data = foerderkatalog.to_dict('records')

    for item in data:
        item['Laufzeit von'] = item['Laufzeit von'].to_pydatetime()
        item['Laufzeit bis'] = item['Laufzeit bis'].to_pydatetime()

    result = [(d['FKZ'], d['Ressort'], d['Zuwendungsempfänger'], d['Stadt/Gemeinde'], d['Bundesland'],
               d['Ausführende Stelle'], d['Stadt/Gemeinde.1'], d['Bundesland.1'], d['Thema'], d['Laufzeit von'],
               d['Laufzeit bis'], d['Fördersumme in EUR'], d['Verbundprojekt']) for d in data]

    try:
        cursor.executemany(
            """INSERT IGNORE INTO """ + table + """(FKZ,Ressort,Zuwendungsempfänger,Stadt_Zuwendungsempfänger,
            Bundesland_Zuwendungsempfänger,`Ausführende Stelle`,`stadt_Ausführende Stelle`,`Bundesland_Ausführende Stelle`,
            Thema,`Laufzeit von`,`Laufzeit bis`,`Fördersumme in EUR`, Verbundprojekt) VALUES(%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
            %s,%s)""", result)
    except Exception as err:
        print(err)


# Tabellen mit jeweiligen Name erstellen
createTable(cur, database)

# Load Vorfilterungskatalog aus der csv-Datei (data/schlüsselbegriff.csv) nur die erste Spalte
loadVorfilterung(path_schluesselbegriffe_csv, 'Table_Vordefinierte_Katalog', cur)

# Load die gesamte Filterungskatalogen aus der csv-Datei (data/schlüsselbegriff.csv) von 1.1 bis 4.6
loadKatalog(path_schluesselbegriffe_csv, 'Table_Katalog', cur)

# load die Förderprogrammdaten mit Crawler und in Datentabelle speichern
loadFdorderprojekt(path_foerderkatalog_csv, 'Table_Förderkatalog', cur)

# noch mal aufrufen der Liste der Vorfilterung und Beziehung zwischen
# Table_Förderkatalog und Table_vordefinierte_Katalog herstellen
cur.execute("SELECT * FROM Table_Vordefinierte_Katalog")
Vorfilterung_List = [x[0] for x in cur.fetchall()]
Vorfilterung_List = [re.sub(r"_", " ", item, re.I) for item in Vorfilterung_List]
for item in Vorfilterung_List:
    sql = """INSERT IGNORE INTO Table_Relation_VorD_TH(förderkatalog_id, vordefinierte_katalog_id) SELECT E.id, F.id FROM Table_Förderkatalog E, Table_Vordefinierte_Katalog F  WHERE E.Thema LIKE """ + """ '%%""" + item + """%%' """ + """AND F.Katalog='""" + item + "'"
    cur.execute(sql)

# noch mal aufrufen der Liste der gesamten Filterung und Beziehung zwischen
# Table_Förderkatalog und Table_Katalog herstellen
cur.execute("SELECT * FROM Table_Katalog")
Katalog_List = [x[0] for x in cur.fetchall()]
Katalog_List = [re.sub(r"_", " ", item, re.I) for item in Katalog_List]
for item in Katalog_List:
    sql = """INSERT IGNORE INTO Table_Relation_KA_TH(förderkatalog_id, Katalog_id) SELECT F.id, G.id FROM 
    Table_Förderkatalog F, Table_Katalog G  WHERE F.id in (SELECT E.Förderkatalog_id FROM Table_Relation_VorD_TH E) 
    AND F.Thema LIKE """ + """ '%%""" + item + """%%' """ + """AND G.Katalog='""" + item + "'"
    cur.execute(sql)

# Änderung des Datenbank abgeben und Verbindung aufhören
db.commit()
cur.close()
db.close()
