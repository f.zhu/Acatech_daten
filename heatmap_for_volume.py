#!/usr/bin/env python
# coding: utf-8
from datetime import date
from multiprocessing import Pool, Manager, cpu_count

import pymysql

from visualisierung import *
from spans import daterange


sns.set()
# Benutzer Info für Datenbank
database = 'data_AcaTech'
user = 'root'
host = 'localhost'
password = '1234'

BIP_Bundesland_2019_Mio_EUR = {"Baden-Württemberg": 524325,
                               "Bayern": 632897,
                               "Berlin": 153291,
                               "Brandenburg": 74330,
                               "Bremen": 33623,
                               "Hamburg": 123270,
                               "Hessen": 294477,
                               "Mecklenburg-Vorpommern": 46567,
                               "Niedersachsen": 307036,
                               "Nordrhein-Westfalen": 711419,
                               "Rheinland-Pfalz": 145003,
                               "Saarland": 36253,
                               "Sachsen": 128097,
                               "Sachsen-Anhalt": 63545,
                               "Schleswig-Holstein": 97762,
                               "Thüringen": 63866}

db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database)
sql = """SELECT A.id, A.`Fördersumme in EUR`, A.Thema, A.Bundesland_Zuwendungsempfänger, A.Ressort,
    A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM Table_Förderkatalog A WHERE id IN (SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH);"""
cur.execute(sql)
results = cur.fetchall()
# es wird hier nur teilweise exportiert aus Datenbank
df_KI = pd.DataFrame(results, columns=["id", "Fördersumme in EUR", "Thema",
                                       "Bundesland_Zuwendungsempfänger", "Ressort", "Laufzeit von",
                                       "Laufzeit bis", "Verbundprojekt"])

# bescränken auf Laufzeit bis > 2020.1.1
deadlinelimit = 2020
datalist_new_zeitfilter = df_KI[(df_KI["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1))]
target_reduction_daterange = daterange(datetime.date(2019, 1, 1), datetime.date(2019, 12, 31))

time_diff_list = []
time_diff_list_against_2019 = []
for i in range(datalist_new_zeitfilter.__len__()):
    data_daterange = daterange(datalist_new_zeitfilter.iloc[i]["Laufzeit von"], datalist_new_zeitfilter.iloc[i]["Laufzeit bis"])
    daterange_intersection = data_daterange.intersection(target_reduction_daterange)
    time_diff = data_daterange.last - data_daterange.lower + timedelta(2)
    try:
        time_diff_against_2019 = daterange_intersection.last - daterange_intersection.lower + timedelta(2)
    except TypeError:
        time_diff_against_2019 = timedelta(0)
    time_diff_list_against_2019.append(time_diff_against_2019.days//30)
    time_diff_list.append(time_diff.days//30)

datalist_new_zeitfilter["month_intersection"] = time_diff_list_against_2019
datalist_new_zeitfilter["month"] = time_diff_list
datalist_new_zeitfilter["Fördersumme in EUR reduziert auf 2019"] = datalist_new_zeitfilter["Fördersumme in EUR"] * (datalist_new_zeitfilter["month_intersection"]/datalist_new_zeitfilter["month"])

group = datalist_new_zeitfilter.groupby("Bundesland_Zuwendungsempfänger")
Bundesland_data = []

for i, partdataframe in enumerate(group):
    Bundesland_data.append((partdataframe[0], partdataframe[1]["Fördersumme in EUR reduziert auf 2019"].sum()/10e+5, (partdataframe[1]["Fördersumme in EUR reduziert auf 2019"].sum()/10e+5)/BIP_Bundesland_2019_Mio_EUR[partdataframe[0]]*100))

Bundesland_data = tuple(Bundesland_data)
Bundesland_data = pd.DataFrame(Bundesland_data, columns=['Bundesland', 'Fördersumme in Mio. EUR reduziert auf 2019', 'Fördersumme normiert auf BIP in prozent'])
deu_geo = 'data/bundeslaender_simplify200.geojson'
deu_map = folium.Map(location=(52, 10), zoom_start=6)
folium.GeoJson(
    deu_geo,
    style_function=lambda feature: {
        'fillColor': '#ffff00',
        'color': 'black',
        'weight': 2,
        'dashArray': '5, 5'
    }
).add_to(deu_map)

folium.Choropleth(
    geo_data=deu_geo,
    data=Bundesland_data,
    columns=['Bundesland', 'Fördersumme in Mio. EUR reduziert auf 2019'],
    key_on='feature.properties.name',
    fill_color='YlGnBu',
    bins=[0,20,40,60,80,100,120,140,160,180],
    fill_opacity=0.7,
    line_opacity=0.3,
    highlight=True,
    legend_name='Fördersumme von KI-Projekten 2019 in den Bundesländern in Mio. Euro'
).add_to(deu_map)

# display map
deu_map.save('figure/new_25.11.2020/KI_Projekten_Fördervolumen.html')

deu_map1 = folium.Map(location=(52, 10), zoom_start=6)
folium.GeoJson(
    deu_geo,
    style_function=lambda feature: {
        'fillColor': '#ffff00',
        'color': 'black',
        'weight': 2,
        'dashArray': '5, 5'
    }
).add_to(deu_map1)

folium.Choropleth(
    geo_data=deu_geo,
    data=Bundesland_data,
    columns=['Bundesland', 'Fördersumme normiert auf BIP in prozent'],
    key_on='feature.properties.name',
    fill_color='YlGnBu',
    bins=[0,0.005,0.01,0.015,0.02,0.025,0.03,0.035,0.04,0.045],
    fill_opacity=0.7,
    line_opacity=0.3,
    highlight=True,
    legend_name='Fördersumme von KI-Projekten 2019 normiert auf das jeweilige BIP in % in den Bundesländern'
).add_to(deu_map1)

# display map
deu_map1.save('figure/new_25.11.2020/KI_Projekten_Fördervolumen_prozent.html')

