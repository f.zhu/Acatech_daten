#!/usr/bin/env python
# coding: utf-8
from datetime import date
from multiprocessing import Pool, Manager, cpu_count

import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database0 = 'data_AcaTech'
database1 = 'data_förderprogramm'
user = 'root'
host = 'localhost'
password = '1234'

# Verbindung zum Datenbank herstellen
db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database0)
sql1 = """SELECT Katalog FROM Table_Vordefinierte_Katalog"""
cur.execute(sql1)
vordefinierte_katalog = [x[0] for x in cur.fetchall()]
vordefinierte_katalog_klartext = ''
for i, begriff in enumerate(vordefinierte_katalog):
    if i == vordefinierte_katalog.__len__() - 1:
        vordefinierte_katalog_klartext += begriff
        break
    vordefinierte_katalog_klartext += begriff + '|'
length = vordefinierte_katalog.__len__()
datalist = []
bundesland_list = ['Nordrhein-Westfalen', 'Niedersachsen', 'Mecklenburg-Vorpommern', 'Hessen', 'Brandenburg', 'Bayern',
                   'Baden-Württemberg', 'Thüringen', 'Sachsen', 'Sachsen-Anhalt', 'Saarland', 'Rheinland-Pfalz',
                   'Schleswig-Holstein', 'Bremen', 'Berlin', 'Hamburg']
cur.execute("USE " + database1)


datalist_new = []
sql = """SELECT * FROM Table_Förderprogramm_details WHERE title REGEXP '""" + vordefinierte_katalog_klartext + """'"""
cur.execute(sql)
results = cur.fetchall()
results = pd.DataFrame(results, columns=["title", "foerderart", "foerdergebiet",
                                         "foerderberechtigte", "foerdergeber", "ansprechpunkt",
                                         "Kurzzusammenfassung", "Zusatzinfos", "Rechtsgrundlage", "href"])
results.drop_duplicates(subset=['title'], inplace=True)
datalist_new.append(results)

anzahl_KI_Bekanntmachungen = results.__len__()


def drawbarplot(data, filter, anzahl_KI_Bekanntmachungen, anmerkung='', amerkung2=''):
    anzahl = data.groupby([filter], as_index=False)[filter].agg(
        {'cnt': 'count'})
    prozent = anzahl.copy()
    prozent['cnt'] = (anzahl['cnt'] / anzahl_KI_Bekanntmachungen) * 100
    prozent = prozent.sort_values(by=['cnt'], ascending=False)
    fig1, ax1 = plt.subplots(figsize=(40, 15))
    sns.barplot(x='cnt', y=filter, data=prozent, ax=ax1, palette="deep")
    plt.xlabel('Prozentzahl', fontsize=15, weight='bold')
    plt.ylabel(filter, fontsize=15, weight='bold')
    ax1.set_title(filter + ' an % aller KI-Bekanntmachungen' + amerkung2, fontsize=20, weight='bold')
    plt.savefig('figure/Ressort_Katalog/' + anmerkung + '_förderprogramm_' + filter + '.svg', format='svg',
                transparent=True)


datalist_new_foerderart = []
for item in datalist_new:
    if item.__len__() != 0:
        aaa = item.dropna(subset=['foerderart'])
        _temp = pd.concat(
            [pd.Series(row['title'], row['foerderart'].split(', ')) for _, row in aaa.iterrows()]).reset_index()
        _temp.columns = ['foerderart', 'title']
        datalist_new_foerderart.append(_temp)
# Anteil jeweiliger Foerderart von aller KI-Bekanntmachungen
drawbarplot(datalist_new_foerderart[0], 'foerderart', anzahl_KI_Bekanntmachungen, anmerkung='gesamt')

datalist_new_foerderberechtigte = []
for item in datalist_new:
    if item.__len__() != 0:
        aaa = item.dropna(subset=['foerderberechtigte'])
        _temp = pd.concat(
            [pd.Series(row['title'], row['foerderberechtigte'].split(', ')) for _, row in aaa.iterrows()]).reset_index()
        _temp.columns = ['foerderberechtigte', 'title']
        datalist_new_foerderberechtigte.append(_temp)
# Anteil jeweiliger foerderberechtigte von aller KI-Bekanntmachungen
drawbarplot(datalist_new_foerderberechtigte[0], 'foerderberechtigte', anzahl_KI_Bekanntmachungen, anmerkung='gesamt')

datalist_new_foerdergeber = []
for item in datalist_new:
    if item.__len__() != 0:
        aaa = item.dropna(subset=['foerdergeber']).reset_index(drop=True)
        _temp = pd.concat(
            [pd.Series(row['title'], row['foerdergeber'].split('; ')) for _, row in aaa.iterrows()]).reset_index()
        _temp.columns = ['foerdergeber', 'title']
        datalist_new_foerdergeber.append(_temp)

order = datalist_new_foerdergeber[0]['foerdergeber'].value_counts().reset_index()
gross_foerdergeber = order.iloc[0:6]['index'].tolist()
datalist_new_foerdergeber[0]['foerdergeber'][
    ~datalist_new_foerdergeber[0]['foerdergeber'].isin(gross_foerdergeber)] = 'sonstiges'
# Anteil jeweiliger foerdergeber von aller KI-Bekanntmachungen
pieChartRessort(datalist_new_foerdergeber, filter='foerdergeber', name='förderprogramm_fördergeber',
                DataframeList=False)
plt.close('all')

########################################################################################################################
cur.execute("""SELECT MAX(index2) FROM table_filters_foerderdatenbank WHERE index1=1;""")
foerderbereich_anzahl = cur.fetchone()[0]
datalist_new_foerderbereich = []

for i in range(1, foerderbereich_anzahl + 1):
    sql = """SELECT A.name, A.index1, A.index2, B.title, B.foerderart, B.foerdergebiet, B.foerderberechtigte, B.foerdergeber, B.ansprechpunkt, B.Kurzzusammenfassung, B.Zusatzinfos, B.Rechtsgrundlage, B.href FROM table_filters_foerderdatenbank A, """ + """(SELECT * FROM Table_Förderprogramm_details WHERE title REGEXP '""" + vordefinierte_katalog_klartext + """') B , table_foerderdatenbank C WHERE A.index1=1 AND A.index2=""" + str(
        i) + """ AND C.href=B.href AND C.filter_id=A.id;"""
    cur.execute(sql)
    results = cur.fetchall()
    if results.__len__() != 0:
        _temp = pd.DataFrame(results,
                             columns=["vordefinierte Katalog", "index1", "index2", "title", "foerderart", "foerdergebiet",
                                      "foerderberechtigte", "foerdergeber", "ansprechpunkt",
                                      "Kurzzusammenfassung", "Zusatzinfos", "Rechtsgrundlage", "href"])
    else:
        print('es gibt keine Bekanntmachungen zu dem Katalog ' + str(i))
        continue
    _temp['vordefinierte Katalog'] = _temp.iloc[0]['vordefinierte Katalog']
    # es wird hier nur teilweise exportiert aus Datenbank
    datalist_new_foerderbereich.append(_temp.drop_duplicates('title', 'first'))

df_foerderbereich = pd.concat(datalist_new_foerderbereich)

# der Anteil jeweiliger Katalog von aller KI-Bekanntmachungen !!!
drawbarplot(df_foerderbereich, 'vordefinierte Katalog', anzahl_KI_Bekanntmachungen, anmerkung='gesamt')

for item in datalist_new_foerderbereich:
    if item.__len__() != 0:
        aaa = item.dropna(subset=['foerderberechtigte'])
        anzahl_temp = aaa.drop_duplicates('title', 'first').__len__()
        _temp = pd.concat(
            [pd.Series(row['title'], row['foerderberechtigte'].split(', ')) for _, row in aaa.iterrows()]).reset_index()
        _temp.columns = ['foerderberechtigte', 'title']
        _temp['vordefinierte Katalog'] = aaa.iloc[0]['vordefinierte Katalog']
        # Anteil jeweiliger foerderberechtigte von aller KI-Bekanntmachung jeweiliger Katalog
        drawbarplot(_temp, 'foerderberechtigte', anzahl_temp,
                    anmerkung=aaa.iloc[0]['vordefinierte Katalog'], amerkung2=""" von dem Katalog \"""" + aaa.iloc[0]['vordefinierte Katalog']+"""\"""")

for item in datalist_new_foerderbereich:
    if item.__len__() != 0:
        aaa = item.dropna(subset=['foerderart'])
        anzahl_temp = aaa.drop_duplicates('title', 'first').__len__()
        _temp = pd.concat(
            [pd.Series(row['title'], row['foerderart'].split(', ')) for _, row in aaa.iterrows()]).reset_index()
        _temp.columns = ['foerderart', 'title']
        _temp['vordefinierte Katalog'] = aaa.iloc[0]['vordefinierte Katalog']
        # Anteil jeweiliger foerderart von aller KI-Bekanntmachung jeweiliger Katalog
        drawbarplot(_temp, 'foerderart', anzahl_temp,
                    anmerkung=aaa.iloc[0]['vordefinierte Katalog'], amerkung2=""" von dem Katalog \"""" + aaa.iloc[0]['vordefinierte Katalog']+"""\"""")

for item in datalist_new_foerderbereich:
    if item.__len__() != 0:
        bbb = item.dropna(subset=['foerdergebiet'])
        anzahl_temp = bbb.drop_duplicates('title', 'first').__len__()
        _temp = pd.concat(
            [pd.Series(row['title'], row['foerdergebiet'].split(', ')) for _, row in bbb.iterrows()]).reset_index()
        _temp.columns = ['foerdergebiet', 'title']
        vereinigen_bundesland = pd.DataFrame(columns=_temp.columns)
        for w in range(_temp.__len__()):
            if _temp.iloc[w]['foerdergebiet'] == 'bundesweit':
                aaa = pd.DataFrame(columns=_temp.columns)
                aaa['foerdergebiet'] = bundesland_list
                aaa['title'] = _temp.iloc[w]['title']
                vereinigen_bundesland = pd.concat([vereinigen_bundesland, aaa], axis=0, ignore_index=True)
        _temp = pd.concat([_temp, vereinigen_bundesland])
        _temp.drop(index=_temp[(_temp['foerdergebiet'] == 'bundesweit') | (_temp['foerdergebiet'] == 'sonstige')].index,
                   inplace=True)
        # Anteil jeweiliger foerdergebiet von aller KI-Bekanntmachung jeweiliger Katalog
        drawbarplot(_temp, 'foerdergebiet', anzahl_temp,
                    anmerkung=bbb.iloc[0]['vordefinierte Katalog'], amerkung2=""" von dem Katalog \"""" + bbb.iloc[0]['vordefinierte Katalog']+"""\"""")

datalist_jointplot = pd.DataFrame(columns=['foerderberechtigte', 'vordefinierte Katalog', 'anzahl'])
for item in datalist_new_foerderbereich:
    if item.__len__() != 0:
        aaa = item.dropna(subset=['foerderberechtigte'])
        _temp = pd.concat(
            [pd.Series(row['title'], row['foerderberechtigte'].split(', ')) for _, row in aaa.iterrows()]).reset_index()
        _temp.columns = ['foerderberechtigte', 'title']
        _temp['vordefinierte Katalog'] = aaa.iloc[0]['vordefinierte Katalog']
        group = _temp.groupby("foerderberechtigte")
        for i, partdataframe in enumerate(group):
            datalist_jointplot = datalist_jointplot.append(
                {'foerderberechtigte': partdataframe[1].iloc[0]["foerderberechtigte"],
                 'vordefinierte Katalog': partdataframe[1].iloc[0]["vordefinierte Katalog"],
                 'anzahl': partdataframe[1].__len__()}, ignore_index=True)

heat = datalist_jointplot.pivot("vordefinierte Katalog", "foerderberechtigte", "anzahl")
heat.fillna(0, inplace=True)

f, ax = plt.subplots(figsize=(10, 10))
sns.heatmap(heat, annot=True, linewidths=.5, ax=ax, fmt="d")
ax.set_title("Beziehung zwischen Förderberechtigte und Katalog", fontsize=20, weight='bold')
plt.savefig('figure/förderprogramm_foerderberechtigte_Katalog.svg', bbox_inches='tight', format='svg', transparent=True)

datalist_new_katalog_foerderkatalog = []
cur.execute(
    """SELECT distinct übergeordnete_1, übergeordnete_2, übergeordnete_2_KlarText FROM data_AcaTech.Table_Katalog;""")
aaa = cur.fetchall()
for item in aaa:
    sql1 = """SELECT Katalog FROM data_AcaTech.Table_Katalog WHERE übergeordnete_2_KlarText='""" + item[2] + """'"""
    cur.execute(sql1)
    katalog = [x[0] for x in cur.fetchall()]
    katalog_klartext = ''
    for i, begriff in enumerate(katalog):
        if i == katalog.__len__() - 1:
            katalog_klartext += begriff
            break
        katalog_klartext += begriff + '|'
    sql2 = """SELECT C.title, C.foerderart, C.foerdergebiet,
        C.foerderberechtigte, C.foerdergeber, C.ansprechpunkt, C.Kurzzusammenfassung, C.Zusatzinfos, C.Rechtsgrundlage,
        C.href FROM (SELECT * FROM Table_Förderprogramm_details WHERE title REGEXP '""" + vordefinierte_katalog_klartext + """') C WHERE C.title REGEXP '""" + katalog_klartext + """'"""
    cur.execute(sql2)
    results = cur.fetchall()
    if results.__len__() != 0:
        a = pd.DataFrame(results, columns=["title", "foerderart", "foerdergebiet", "foerderberechtigte", "foerdergeber",
                                           "ansprechpunkt",
                                           "Kurzzusammenfassung", "Zusatzinfos", "Rechtsgrundlage", "href"])
    else:
        print('es gibt keine Bekanntmachungen zu dem Katalog ' + str(item[0]) + '.' + str(item[1]) + ' ' + item[2])
        continue
    a['vordefinierte Katalog'] = str(item[0]) + '.' + str(item[1]) + ' ' + item[2]
    datalist_new_katalog_foerderkatalog.append(a.drop_duplicates('title', 'first'))
df_katalog_foerderkatalog = pd.concat(datalist_new_katalog_foerderkatalog)
drawbarplot(df_katalog_foerderkatalog, 'vordefinierte Katalog', anzahl_KI_Bekanntmachungen,
            anmerkung='gesamt_förderkatalog_katalog')
