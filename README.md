# Acatech_daten

## Anleitung
MySQL 5.7 wird hier eingesetzt und muss installiert werden
### Crawler und Analyser für https://www.pt-ad.pt-dlr.de/de/885.php
**1. durchführen data_foederkatalog_einordnen.py** <br>
**2. durchführen analyse_forderkatalog.py**<br>

### Hier folgend zeigen sich einigen Beispielen anhand der Analyse

**Anzahl der geförderten Programm jenach vordefinierten Katalog**<br>
anzahlKatalog(datalist_new_zeitfilter_drop_verbundprojekt, name='anzahl_jeweiligen_deatilkatalog.png')
![](figure/anzahl_jeweiligen_detailkatalog.png)


**Zeitliche Verläufe der Anzahl von Förderprogramm jenach vordefinierten Katalog**<br>
zeitlicheVerlauf(datalist_new_drop_verbundprojekt, name='zeitliche_Verlauf_detailkatalog.png', start=date(1970, 1, 1), end=date(2020, 12, 31))
![](figure/zeitliche_Verlauf_detailkatalog.png)

**Fördersumme der Programmen jenach vordefinierten Katalog**<br>
summeBoxplot(datalist_new_zeitfilter, name='summe_boxplot_vordkatalog_detail_katalog.png')
![](figure/fördersumme_detailkatalog_boxplot.png)

![](figure/fördersumme_detailkatalog_violinplot.png)

**Wordcloud der Förderprogrammen**
![](figure/wordcloud/KI_wordcloud.png)


### Crawler für www.Förderdatenbank.de
**1. durchführen data_foederprogramm_einordnen.py**<br>
**2. durchführen data_foerderprogramm_detail.py**<br>
**3. durchführen data_foerderprogramm_filters.py**<br>
**4. durchführen analyse_foerderprogramm.py**



### ToDo List

- [x] Optimierung der Geschwindigkeit von Crawler mit multiprocessing
- [x] Einführung der Datenbank für Förderprogramm und Förderkatalog
- [x] Analyse der Daten von https://www.pt-ad.pt-dlr.de/de/885.php
- [ ] Analyse der Daten von https://www.Förderdatenbank.de
- [x] Erstellung eines Bar Charts der die Anzahl von Förderprojekten für die Unterkategorien zeigt (1.1, 1.2, 1.3 etc.) (Siehe pptx)
- [x] Erstellung eines Box Plots der die Fördersumme von Förderprojekten für die Unterkategorien zeigt (1.1, 1.2, 1.3 etc.) (Siehe pptx)
- [ ] Erstellung eines Bar Charts der die Anzahl von Förderprogrammen (aus Förderdatenbank) für die Unterkategorien zeigt (1.1, 1.2, 1.3 etc.) (Siehe pptx)
- [ ] Erstellung eines Box Plots der die Fördersumme von Förderprogrammen (aus Förderdatenbank) für die Unterkategorien zeigt (1.1, 1.2, 1.3 etc.) (Siehe pptx)
- [ ] Heatmap mit Unterkategorien von Unternehmen (Kleinstunternehmen, Mittlere Unternehmen etc.)
- [x] Optimierung der Datenstruktur der Daten(Filter) aus https://www.Förderdatenbank.de (nun als dict, vlleicht auch als Datenbank-Table)
- [ ] Weitere Möglichkeit für Analyse der Daten 

