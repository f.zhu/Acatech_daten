#!/usr/bin/env python
# coding: utf-8

import time
from multiprocessing import Pool, Manager, cpu_count

import pymysql
import requests
from bs4 import BeautifulSoup

# Benutzer Info für Datenbank
database = "data_förderprogramm"
user = 'root'
host = 'localhost'
password = '1234'
base_URL = "https://www.foerderdatenbank.de/"


class OP:
    def __init__(self, all_program_refs):
        self.manager = Manager
        self.mp_list = self.manager().list()
        self.data = all_program_refs

    def proc_func(self, i, ref):
        # set_trace() # Falls debug benötigt wird
        print("Working on Foerderprogram {} of {}".format(i + 1, len(self.data)))
        page_url = base_URL + ref
        page = requests.get(page_url)
        soup = BeautifulSoup(page.content, 'html.parser')

        foerderprogram_details = {'foerderart': None, 'foerdergebiet': None, 'foerderberechtigte': None,
                                  'foerdergeber': None, 'ansprechpunkt': None, 'Kurzzusammenfassung': None,
                                  'Zusatzinfos': None, 'Rechtsgrundlage': None, 'href': None,
                                  "title": soup.find("h1", class_="title").text.strip()}

        # Get "metadata"
        metadata = soup.find("dl", class_="grid-modul--two-elements document-info-fundingprogram")
        field_names = metadata.findAll("dt")
        field_values = metadata.findAll("dd")

        for name, value in zip(field_names, field_values):
            if "Förderart" in name.text:
                foerderprogram_details["foerderart"] = value.text.strip()
            if "Fördergebiet" in name.text:
                foerderprogram_details["foerdergebiet"] = value.text.strip()
            if "Förderberechtigte" in name.text:
                foerderprogram_details["foerderberechtigte"] = value.text.strip()
            if "Fördergeber" in name.text:
                foerderprogram_details["foerdergeber"] = value.text.strip()
            if "Ansprechpunkt" in name.text:
                foerderprogram_details["ansprechpunkt"] = value.text.strip()

        # Get content
        content_tab = soup.find("div", class_="container content--main for-aside-right horizontal--tab")
        # content_single = soup.find("div", class_="container content--main for-aside-right")

        if content_tab is not None:
            # content = {}
            content_tab_titles = content_tab.findAll("h2", class_="horizontal--tab-opener")
            content_tabs = content_tab.findAll("article", class_="content--tab-text")
            for title, tab in zip(content_tab_titles, content_tabs):
                tab_title = title.find("span", class_="btn--label").text.strip()
                tab_content = tab.text.strip()
                foerderprogram_details[tab_title] = tab_content
        foerderprogram_details['href'] = ref
        db = pymysql.connect(host, user, password)
        cur = db.cursor(pymysql.cursors.DictCursor)
        cur.execute("USE " + database)
        self.mp_list.append(foerderprogram_details)

    def flow(self):
        pool = Pool(cpu_count())
        for i in range(len(self.data)):
            pool.apply_async(self.proc_func, args=(i, self.data[i]))
        pool.close()
        pool.join()


if __name__ == '__main__':
    # Verbindung zum Datenbank herstellen
    db = pymysql.connect(host, user, password)
    cur = db.cursor(pymysql.cursors.DictCursor)
    cur.execute("USE " + database)

    # url aus Datenbank ablesen, um später multiprocessing crawler zu bilden
    cur.execute("SELECT * FROM Table_Förderprogramm")
    all_program_refs = [x['Thema_Link_html'] for x in cur.fetchall()]

    # Tabelle erstellen, um später die Daten zu speichern
    try:
        sql = """CREATE TABLE Table_Förderprogramm_details (
                             title                   TEXT       NOT NULL,
                             foerderart              TEXT       NULL,
                             foerdergebiet           TEXT       NULL,
                             foerderberechtigte      TEXT       NULL,
                             foerdergeber            TEXT       NULL,
                             ansprechpunkt           TEXT       NULL,
                             Kurzzusammenfassung     TEXT       NULL,
                             Zusatzinfos             TEXT       NULL,
                             Rechtsgrundlage         TEXT       NULL,
                             href                    VARCHAR(255)       NOT NULL,
                            CONSTRAINT contacts_pk PRIMARY KEY (href)
                             );"""
        # sql0 = """ALTER TABLE Table_Förderprogramm_details ADD id INT NOT NULL PRIMARY KEY AUTO_INCREMENT;"""  # add primary key
        # sql1 = """ALTER TABLE Table_Förderprogramm_details ADD UNIQUE KEY(href(200));"""  # unique constrain
        cur.execute(sql)
        # cur.execute(sql0)
        # cur.execute(sql1)
        db.commit()
    except:  # falls Tabelle schon vorhanden, pass
        print(pymysql.err.InternalError)
        pass

    start_time = time.time()
    op = OP(all_program_refs)
    op.flow()
    # Datensammlung von der multiprocessing.manager.list() aufrufen
    foerderprogramme = op.mp_list
    # List proxy to List
    data = foerderprogramme[:]

    # dict to tuple, um in den Datenbank hochzuladen
    result = [(d['title'], d['foerderart'], d['foerdergebiet'], d['foerderberechtigte'], d['foerdergeber'],
               d['ansprechpunkt'], d['Kurzzusammenfassung'], d['Zusatzinfos'],
               d['Rechtsgrundlage'], d['href']) for d in data]

    # Daten hochladen
    try:
        cur.executemany(
            """INSERT IGNORE INTO Table_Förderprogramm_details(title, foerderart, foerdergebiet,foerderberechtigte, 
            foerdergeber, ansprechpunkt,Kurzzusammenfassung,Zusatzinfos,Rechtsgrundlage,href) VALUES(%s,%s,%s,%s,%s,%s,
            %s,%s,%s,%s)""",
            result)
    except Exception as err:
        print(err)

    db.commit()
    cur.close()
    db.close()
