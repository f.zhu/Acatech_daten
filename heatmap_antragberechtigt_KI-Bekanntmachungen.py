#!/usr/bin/env python
# coding: utf-8
from datetime import date
from multiprocessing import Pool, Manager, cpu_count

import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database0 = 'data_AcaTech'
database1 = 'data_förderprogramm'
user = 'root'
host = 'localhost'
password = '1234'

# Verbindung zum Datenbank herstellen
db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database0)
sql1 = """SELECT Katalog FROM Table_Vordefinierte_Katalog"""
cur.execute(sql1)
vordefinierte_katalog = [x[0] for x in cur.fetchall()]
vordefinierte_katalog_klartext = ''
for i, begriff in enumerate(vordefinierte_katalog):
    if i == vordefinierte_katalog.__len__()-1:
        vordefinierte_katalog_klartext += begriff
        break
    vordefinierte_katalog_klartext += begriff + '|'
length = vordefinierte_katalog.__len__()
datalist = []

cur.execute("USE " + database1)

sql = """SELECT A.title, B.name FROM (SELECT * FROM Table_Förderprogramm_details WHERE title REGEXP '""" + vordefinierte_katalog_klartext + """') A , table_filters_foerderdatenbank B, table_foerderdatenbank C WHERE B.index1=2 AND C.filter_id=B.id AND A.href=C.href AND NOT B.name='bundesweit';"""

cur.execute(sql)
results = cur.fetchall()

df_KI = pd.DataFrame(results, columns=["Thema", "Bundesland_Zuwendungsempfänger"])
# Prorammen zählen
anzahl_KI_Bekanntmachungen = df_KI.drop_duplicates('Thema', 'first').__len__()

group = df_KI.groupby("Bundesland_Zuwendungsempfänger" )

Bundesland_data = []
for i, partdataframe in enumerate(group):
    land = partdataframe[0].title()
    if land == 'Baden-Wuerttemburg':
        land = land.replace('ue', 'ü')
        land = land.replace('burg', 'berg')
    elif land == 'Thueringen':
        land = land.replace('ue', 'ü')
    Bundesland_data.append((land, (partdataframe[1].drop_duplicates('Thema', 'first').__len__() / anzahl_KI_Bekanntmachungen) * 100))
Bundesland_data = tuple(Bundesland_data)
Bundesland_data = pd.DataFrame(Bundesland_data, columns=['Bundesland', 'Anteil in %'])

fig1, ax1 = plt.subplots(figsize=(15, 10))
sns.barplot(x='Anteil in %', y='Bundesland', data=Bundesland_data, order=Bundesland_data.sort_values('Anteil in %', ascending=False).Bundesland, ax=ax1, palette="rocket")
ax1.set_xlabel('Prozentzahl')
ax1.set_title('Anteil der berechtigten KI-Bekanntmachungen in Prozentzahl', fontsize=20, weight='bold')
plt.savefig('figure/new_25.11.2020/Anteil_KI-Bekanntmachungen_prozentzahl_barplot.svg',
            bbox_inches='tight', format='svg', transparent=True)

deu_geo = 'data/bundeslaender_simplify200.geojson'
deu_map = folium.Map(location=(52, 10), zoom_start=6)
folium.GeoJson(
    deu_geo,
    style_function=lambda feature: {
        'fillColor': '#ffff00',
        'color': 'black',
        'weight': 2,
        'dashArray': '5, 5'
    }
).add_to(deu_map)
folium.Choropleth(
    geo_data=deu_geo,
    data=Bundesland_data,
    columns=['Bundesland', 'Anteil in %'],
    key_on='feature.properties.name',
    bins=[28,29,30,31,32,33,34,35,36,37],
    fill_color='YlGnBu',
    fill_opacity=0.7,
    line_opacity=0.2,
    highlight=True,
    legend_name='Antragsberechtigt an % aller KI-Förderprogramme'
).add_to(deu_map)

# display map
deu_map.save('figure/new_25.11.2020/Anteil_KI-Bekanntmachungen_prozentzahl_heatmap.html')
