#!/usr/bin/env python
# coding: utf-8

import math
import random
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from time import sleep
import os


def get_foerderprogram_hrefs(soup):
    this_program_refs = []
    results = soup.find("div", class_="searchresult")
    foerderprogram_hits = results.find_all('p', class_='card--title')
    for hit in foerderprogram_hits:
        this_program_refs.append(hit.find('a')["href"])
    return this_program_refs


def downloadFromFoerderkatalog():
    if not os.path.exists('Suchliste.csv'):
        options = webdriver.ChromeOptions()
        prefs = {'profile.default_content_settings.popups': 0, 'download.default_directory': os.getcwd()}
        options.add_experimental_option('prefs', prefs)
        #options.add_argument('--headless')  # headless Chrome
        driver = webdriver.Chrome(options=options)
        driver.get('https://foerderportal.bund.de/foekat/jsp/SucheAction.do?actionMode=searchreset')
        driver.find_element_by_xpath("//input[@name='submitAction' and @value='Schnellsuche starten']").click()
        WebDriverWait(driver, 200, 0.05).until(lambda x: x.find_element_by_class_name('content'))
        driver.find_element_by_xpath("//a[@title='Link öffnet ein neues Fenster für die Ausgabe als Textdatei']").click()
        sleep(3)
        t = driver.switch_to_alert()
        t.accept()
        while not os.path.exists('Suchliste.csv'):
            sleep(5)
        driver.quit()
