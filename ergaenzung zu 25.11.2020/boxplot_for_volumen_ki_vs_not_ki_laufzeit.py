#!/usr/bin/env python
# coding: utf-8
from datetime import date
from multiprocessing import Pool, Manager, cpu_count

import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database = 'data_AcaTech'
user = 'root'
host = 'localhost'
password = '1234'

db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database)
deadlinelimit = 2020
startlimit = 2014

# select die Projekten in der Vorfilterungsklasse
sql = """SELECT A.id, A.`Fördersumme in EUR`, A.Thema, A.Bundesland_Zuwendungsempfänger, A.Ressort,
    A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM Table_Förderkatalog A WHERE id IN (SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH);"""
cur.execute(sql)
results = cur.fetchall()
# es wird hier nur teilweise exportiert aus Datenbank
df_KI = pd.DataFrame(results, columns=["id", "Fördersumme in EUR", "Thema",
                                       "Bundesland_Zuwendungsempfänger", "Ressort", "Laufzeit von",
                                       "Laufzeit bis", "Verbundprojekt"])
df_KI.drop_duplicates(subset=['id'], inplace=True)
# select die Projekten nicht in der Vorfilterungsklasse
sql = """SELECT A.id, A.`Fördersumme in EUR`, A.Thema, A.Bundesland_Zuwendungsempfänger, A.Ressort,
    A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM Table_Förderkatalog A WHERE id NOT IN (SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH);"""
cur.execute(sql)
results = cur.fetchall()
# es wird hier nur teilweise exportiert aus Datenbank
df_NOT_KI = pd.DataFrame(results, columns=["id", "Fördersumme in EUR", "Thema",
                                           "Bundesland_Zuwendungsempfänger", "Ressort", "Laufzeit von",
                                           "Laufzeit bis", "Verbundprojekt"])


df_KI = df_KI[(df_KI["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1)) & (df_KI["Laufzeit von"] > datetime.date(startlimit, 1, 1))]
df_NOT_KI = df_NOT_KI[(df_NOT_KI["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1)) & (df_NOT_KI["Laufzeit von"] > datetime.date(startlimit, 1, 1))]

df_KI['Dauer in Monat'] = ((np.array([x.year for x in df_KI["Laufzeit bis"]]) - np.array(
    [x.year for x in df_KI["Laufzeit von"]])) * 12) + \
                          np.array([x.month for x in df_KI["Laufzeit bis"]]) - np.array(
    [x.month for x in df_KI["Laufzeit von"]]) + 1

df_NOT_KI['Dauer in Monat'] = ((np.array([x.year for x in df_NOT_KI["Laufzeit bis"]]) - np.array(
    [x.year for x in df_NOT_KI["Laufzeit von"]])) * 12) + \
                              np.array([x.month for x in df_NOT_KI["Laufzeit bis"]]) - np.array(
    [x.month for x in df_NOT_KI["Laufzeit von"]]) + 1

# Berechnenung der Foerdersumme und in Mio. EUR umrechnen
class OP1:
    def __init__(self):
        self.manager = Manager
        self.mp_list = self.manager().list()

    def proc_func(self, partdataframe):
        # set_trace()
        if partdataframe[1].__len__() == 1:
            self.mp_list.append(partdataframe[1].to_dict('records')[0])
        else:
            _temp2 = pd.DataFrame([partdataframe[1].iloc[0]])
            _temp2['Fördersumme in EUR'] = partdataframe[1]["Fördersumme in EUR"].sum()
            self.mp_list.append(_temp2.to_dict('records')[0])

    def flow(self):
        pool = Pool(cpu_count())
        for i, partdataframe in enumerate(_temp1):
            pool.apply_async(self.proc_func, args=(partdataframe,))
        pool.close()
        pool.join()

df_KI1 = df_KI[df_KI['Verbundprojekt'].isnull()]
df_KI1.reset_index(inplace=True, drop=True)
# verbundprojekt
_temp = df_KI[~df_KI['Verbundprojekt'].isnull()]
_temp.reset_index(inplace=True, drop=True)
# klassifizieren der Verbundprojekten
_temp1 = _temp.groupby('Verbundprojekt')
op1 = OP1()
op1.flow()
lst1 = op1.mp_list
del _temp1, op1
df_temp = pd.DataFrame(lst1[:])
df_KI1 = pd.concat([df_KI1, df_temp], axis=0, ignore_index=True)
q_low = df_KI1["Fördersumme in EUR"].quantile(0.01)
q_hi = df_KI1["Fördersumme in EUR"].quantile(0.99)
df_KI_verbund_zusammengefasst = df_KI1[(df_KI1["Fördersumme in EUR"] < q_hi) & (df_KI1["Fördersumme in EUR"] > q_low)]
df_KI_verbund_zusammengefasst.loc[:, 'Fördersumme in Mio. EUR'] = df_KI_verbund_zusammengefasst["Fördersumme in EUR"] / 10e+5

df_NOT_KI1 = df_NOT_KI[df_NOT_KI['Verbundprojekt'].isnull()]
df_NOT_KI1.reset_index(inplace=True, drop=True)
# verbundprojekt
_temp = df_NOT_KI[~df_NOT_KI['Verbundprojekt'].isnull()]
_temp.reset_index(inplace=True, drop=True)
# klassifizieren der Verbundprojekten
_temp1 = _temp.groupby('Verbundprojekt')
op1 = OP1()
op1.flow()
lst1 = op1.mp_list
del _temp1, op1
df_temp = pd.DataFrame(lst1[:])
df_NOT_KI1 = pd.concat([df_NOT_KI1, df_temp], axis=0, ignore_index=True)
q_low = df_NOT_KI1["Fördersumme in EUR"].quantile(0.01)
q_hi = df_NOT_KI1["Fördersumme in EUR"].quantile(0.99)
df_NOT_KI_verbund_zusammengefasst = df_NOT_KI1[(df_NOT_KI1["Fördersumme in EUR"] < q_hi) & (df_NOT_KI1["Fördersumme in EUR"] > q_low)]
df_NOT_KI_verbund_zusammengefasst.loc[:, 'Fördersumme in Mio. EUR'] = df_NOT_KI_verbund_zusammengefasst["Fördersumme in EUR"] / 10e+5

# Anzahl der KI- und Kein-KI-Projekten (Hier darf man nicht die Projekten mit exttrem Foerdervolumen weglassen)
fig1, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 10))
sns.barplot(x=['KI Projekt', 'Kein KI Projekt'], y=[df_KI_verbund_zusammengefasst.__len__(), df_NOT_KI_verbund_zusammengefasst.__len__()],
            ax=ax1)

# sns.barplot(x=['KI Projekt', 'Kein KI Projekt'], y=[ki_mean, not_ki_mean], ax=ax2)
df_KI_verbund_zusammengefasst['katalog'] = 'KI Projekt'
df_NOT_KI_verbund_zusammengefasst['katalog'] = 'Kein KI Projekt'
df = pd.concat([df_KI_verbund_zusammengefasst, df_NOT_KI_verbund_zusammengefasst], ignore_index=True)
sns.boxplot(y="Fördersumme in Mio. EUR", x="katalog", data=df, palette="vlag", ax=ax2, showfliers=False)
# sns.stripplot(y="Fördersumme in Mio. EUR", x="katalog", data=df,
#                   size=4, color=".3", linewidth=0, ax=ax2, alpha=0.3)

# sns.barplot(x=['KI Projekt', 'Kein KI Projekt'], y=[ki_mean_dauer, not_ki_mean_dauer], ax=ax3)
df_KI['katalog'] = 'KI Projekt'
df_NOT_KI['katalog'] = 'Kein KI Projekt'

q_low = df_NOT_KI["Dauer in Monat"].quantile(0.01)
q_hi = df_NOT_KI["Dauer in Monat"].quantile(0.99)
df_NOT_KI = df_NOT_KI[(df_NOT_KI["Dauer in Monat"] < q_hi) & (df_NOT_KI["Dauer in Monat"] > q_low)]
#
q_low = df_KI["Dauer in Monat"].quantile(0.01)
q_hi = df_KI["Dauer in Monat"].quantile(0.99)
df_KI = df_KI[(df_KI["Dauer in Monat"] < q_hi) & (df_KI["Dauer in Monat"] > q_low)]

df = pd.concat([df_KI, df_NOT_KI], ignore_index=True)
sns.boxplot(y="Dauer in Monat", x="katalog", data=df, palette="vlag", ax=ax3, showfliers=False)
fig1.subplots_adjust(wspace=0.3, hspace=0)

ax1.set_ylabel("Anzahl")
ax1.set_title("(a) Anzahl der KI- und Kein-KI-Projekten")
ax2.set_ylabel("Fördervolumen")
ax2.set_xlabel('')
ax2.set_title("(b) Verteilung der Fördervolumen in Mio. EUR")
ax3.set_ylabel("Dauer")
ax3.set_xlabel('')
ax3.set_title("(c) Verteilung des Dauers in Monat von der KI- und Kein-KI-Projekten")
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/durchschnittliche_KI_und_Kein_KI.svg',
            bbox_inches='tight', format='svg', transparent=True)
