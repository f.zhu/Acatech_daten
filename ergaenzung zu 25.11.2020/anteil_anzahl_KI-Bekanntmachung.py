#!/usr/bin/env python
# coding: utf-8
from datetime import date
from multiprocessing import Pool, Manager, cpu_count

import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database0 = 'data_AcaTech'
database1 = 'data_förderprogramm'
user = 'root'
host = 'localhost'
password = '1234'

# Verbindung zum Datenbank herstellen
db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database0)
sql1 = """SELECT Katalog FROM Table_Vordefinierte_Katalog"""
cur.execute(sql1)
vordefinierte_katalog = [x[0] for x in cur.fetchall()]
vordefinierte_katalog_klartext = ''
for i, begriff in enumerate(vordefinierte_katalog):
    if i == vordefinierte_katalog.__len__()-1:
        vordefinierte_katalog_klartext += begriff
        break
    vordefinierte_katalog_klartext += begriff + '|'
length = vordefinierte_katalog.__len__()
datalist = []

cur.execute("USE " + database1)
sql = """SELECT distinct A.title FROM (SELECT * FROM Table_Förderprogramm_details WHERE title REGEXP '""" + vordefinierte_katalog_klartext + """') A ;"""
cur.execute(sql)
KI_projekt = cur.fetchall()

sql = """SELECT distinct A.title FROM (SELECT * FROM Table_Förderprogramm_details WHERE NOT title REGEXP '""" + vordefinierte_katalog_klartext + """') A ;"""
cur.execute(sql)
NOT_KI_projekt = cur.fetchall()

fig1, ax1 = plt.subplots(figsize=(10, 10))
sizes = [KI_projekt.__len__(), NOT_KI_projekt.__len__()]
labels = ['KI Projekt', 'Kein KI Projekt']
ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=90)
ax1.set_title('Anteil der KI-Bekanntmachung von alle', fontsize=20, weight='bold')
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/anteil_anzahl_KI-Bekanntmachung.svg',
            bbox_inches='tight', format='svg', transparent=True)
