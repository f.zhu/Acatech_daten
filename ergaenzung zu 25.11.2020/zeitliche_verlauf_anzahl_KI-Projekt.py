#!/usr/bin/env python
# coding: utf-8

from datetime import date
from multiprocessing import Pool, cpu_count, Manager

import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database = 'data_AcaTech'
user = 'root'
host = 'localhost'
password = '1234'

db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database)

sql1 = """SELECT A.Thema, A.`Fördersumme in EUR`, A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM Table_Förderkatalog A WHERE id IN 
(SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH); """
sql2 = """SELECT A.Thema, A.`Fördersumme in EUR`, A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM 
Table_Förderkatalog A WHERE id NOT IN (SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH); """

# select die Projekten in der Vorfilterungsklasse
cur.execute(sql1)
results = cur.fetchall()
# es wird hier nur teilweise exportiert aus Datenbank
df_KI = pd.DataFrame(results, columns=["Thema", "Fördersumme in EUR",
                                       "Laufzeit von",
                                       "Laufzeit bis", "Verbundprojekt"])
cur.execute(sql2)
results = cur.fetchall()
# es wird hier nur teilweise exportiert aus Datenbank
df_NOT_KI = pd.DataFrame(results, columns=["Thema", "Fördersumme in EUR",
                                           "Laufzeit von",
                                           "Laufzeit bis", "Verbundprojekt"])


class OP1:
    def __init__(self):
        self.manager = Manager
        self.mp_list = self.manager().list()

    def proc_func(self, partdataframe):
        # set_trace()
        if partdataframe[1].__len__() == 1:
            self.mp_list.append(partdataframe[1].to_dict('records')[0])
        else:
            _temp2 = pd.DataFrame([partdataframe[1].iloc[0]])
            _temp2['Fördersumme in EUR'] = partdataframe[1]["Fördersumme in EUR"].sum()
            self.mp_list.append(_temp2.to_dict('records')[0])

    def flow(self):
        pool = Pool(cpu_count())
        for i, partdataframe in enumerate(_temp1):
            pool.apply_async(self.proc_func, args=(partdataframe,))
        pool.close()
        pool.join()


df_KI1 = df_KI[df_KI['Verbundprojekt'].isnull()]
df_KI1.reset_index(inplace=True, drop=True)
# verbundprojekt
_temp = df_KI[~df_KI['Verbundprojekt'].isnull()]
_temp.reset_index(inplace=True, drop=True)
# klassifizieren der Verbundprojekten
_temp1 = _temp.groupby('Verbundprojekt')
op1 = OP1()
op1.flow()
lst1 = op1.mp_list
del _temp1, op1
df_temp = pd.DataFrame(lst1[:])
df_KI1 = pd.concat([df_KI1, df_temp], axis=0, ignore_index=True)

df_NOT_KI1 = df_NOT_KI[df_NOT_KI['Verbundprojekt'].isnull()]
df_NOT_KI1.reset_index(inplace=True, drop=True)
# verbundprojekt
_temp = df_NOT_KI[~df_NOT_KI['Verbundprojekt'].isnull()]
_temp.reset_index(inplace=True, drop=True)
# klassifizieren der Verbundprojekten
_temp1 = _temp.groupby('Verbundprojekt')
op1 = OP1()
op1.flow()
lst1 = op1.mp_list
del _temp1, op1
df_temp = pd.DataFrame(lst1[:])
df_NOT_KI1 = pd.concat([df_NOT_KI1, df_temp], axis=0, ignore_index=True)


def zeitlicheVerlauf(item_KI, item_NOT_KI, name, start, end):
    def daterange(start_date, end_date):
        for n in range(0, int((end_date - start_date).days), 30):
            yield start_date + timedelta(n)

    line_data = pd.DataFrame(columns=['zeit', 'Verhältnis'])
    for single_date in daterange(start, end):
        zahl_KI = item_KI[(item_KI['Laufzeit von'] <= single_date) & (item_KI['Laufzeit bis'] >= single_date)].__len__()
        zahl_NOT_KI = item_NOT_KI[
            (item_NOT_KI['Laufzeit von'] <= single_date) & (item_NOT_KI['Laufzeit bis'] >= single_date)].__len__()
        # print(single_date.strftime("%Y-%m-%d"))
        # print(zahl)
        linedata = [[single_date, zahl_KI / (zahl_KI + zahl_NOT_KI)]]
        linedata = tuple(linedata)
        linedata = pd.DataFrame(linedata, columns=['zeit', 'Verhältnis'])
        line_data = pd.concat([line_data, linedata], axis=0, ignore_index=True)

    combined = line_data.apply(pd.to_numeric, errors='ignore')
    _, axes3 = plt.subplots(figsize=(15, 10))
    sns.lineplot(x='zeit', y='Verhältnis', data=combined, ax=axes3, lw=2, palette="Spectral", alpha=0.7)
    plt.xlabel('Zeit', fontsize=15, weight='bold')
    plt.ylabel('Verhältnis', fontsize=15, weight='bold')
    axes3.set_title('Zeitliche Entwicklung der KI-Projekt', fontsize=20, weight='bold')
    plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/' + name + '.svg', bbox_inches='tight',
                format='svg', transparent=True)


zeitlicheVerlauf(df_KI1, df_NOT_KI1, 'zeiliche_verklauf_Ki-Projekt von 2000 bis Ende 2020', start=date(2000, 1, 1), end=date(2020, 12, 31))

