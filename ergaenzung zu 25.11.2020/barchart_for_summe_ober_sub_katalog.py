#!/usr/bin/env python
# coding: utf-8
from datetime import date

import pymysql
from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database = 'data_AcaTech'
user = 'root'
host = 'localhost'
password = '1234'


db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database)
deadlinelimit = 2020

sql1 = """SELECT distinct `übergeordnete_1_Klartext` FROM Table_Katalog"""
cur.execute(sql1)
vordefinierte_katalog = [x[0] for x in cur.fetchall()]
length = vordefinierte_katalog.__len__()
datalist = []
for i, katalog in enumerate(vordefinierte_katalog):
    sql = """SELECT T.id, T.Thema, K.Übergeordnete_1, K.Übergeordnete_2, K.übergeordnete_1_KlarText, 
    K.übergeordnete_2_KlarText, K.Katalog, T.`Fördersumme in EUR`, T.`Laufzeit von`, T.`Laufzeit bis`, 
    T.Bundesland_Zuwendungsempfänger, T.Ressort, T.Verbundprojekt FROM Table_Förderkatalog T, Table_Relation_KA_TH R, 
    Table_Katalog K WHERE T.id=R.Förderkatalog_id AND R.Katalog_id=K.id AND K.übergeordnete_1_KlarText='""" + katalog + """'; """
    cur.execute(sql)
    results = cur.fetchall()
    _temp = pd.DataFrame(results, columns=["id", "Thema", "index1", "index2", "klartext index1", "vordefinierte Katalog",
                                       "unter katalog", "Fördersumme in EUR", "Laufzeit von", "Laufzeit bis",
                                       "Bundesland_Zuwendungsempfänger", "Ressort", "Verbundprojekt"])
    datalist.append(_temp[_temp["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1)])


datalist_oberkategorien = []
for i, item in enumerate(datalist):
    datalist_oberkategorien.append(item.drop_duplicates(subset=['id'], keep='first'))


anzahl_oberkategorien = []
# beim zählen einfach drop duplicated Verbundprojekten
for i, item in enumerate(datalist_oberkategorien):
    not_verbundprojekt = item[item['Verbundprojekt'].isnull()]
    is_verbundprojekt = item[~item['Verbundprojekt'].isnull()]
    is_verbundprojekt.drop_duplicates(subset=['Verbundprojekt'], inplace=True)
    is_and_not_verbundprojekt = pd.concat([not_verbundprojekt, is_verbundprojekt], ignore_index=True)
    anzahl_oberkategorien.append((is_and_not_verbundprojekt.__len__(), str(i+1) + '. ' + vordefinierte_katalog[i]))
anzahl_oberkategorien.sort(reverse=True)

_, ax = plt.subplots(figsize=(15, 10))
sns.barplot(y=[x[1] for x in anzahl_oberkategorien], x=[x[0] for x in anzahl_oberkategorien], ax=ax)
ax.set_xlabel('Anzahl', fontsize=15, weight='bold')
ax.set_title('Anzahl der KI-Projekten aller Ober-Katalogen', fontsize=20, weight='bold')
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/Gesamt_anzahl_KI-Projekten_Ober-Katalog.svg',
            bbox_inches='tight', format='svg', transparent=True)

foerdersumme_oberkategorien = []
# Gesamtsumme, braucht nicht alle Verbundprojekten zu vereinigen
for i, item in enumerate(datalist_oberkategorien):
    foerdersumme_oberkategorien.append((item['Fördersumme in EUR'].sum() / 10e+5, str(i+1) + '. ' + vordefinierte_katalog[i]))
foerdersumme_oberkategorien.sort(reverse=True)

_, ax1 = plt.subplots(figsize=(15, 10))
sns.barplot(y=[x[1] for x in foerdersumme_oberkategorien], x=[x[0] for x in foerdersumme_oberkategorien], ax=ax1)
ax1.set_xlabel('Fördersumme in Mio. EUR', fontsize=15, weight='bold')
ax1.set_title('Gesamt Fördersumme vom Ober-Katalog', fontsize=20, weight='bold')
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/Gesamt_Fördersumme_Ober-Katalog.svg',
            bbox_inches='tight', format='svg', transparent=True)

for i, item in enumerate(datalist):
    group = item.groupby('index2')
    foerdersumme_subkategorien = []
    anzahl_subkategorien = []
    for j, data_subkatalog, in enumerate(group):
        df_subkatalog = data_subkatalog[1].drop_duplicates(subset=['id'], keep='first')
        not_verbundprojekt = df_subkatalog[df_subkatalog['Verbundprojekt'].isnull()]
        is_verbundprojekt = df_subkatalog[~df_subkatalog['Verbundprojekt'].isnull()]
        is_verbundprojekt.drop_duplicates(subset=['Verbundprojekt'], inplace=True)
        is_and_not_verbundprojekt = pd.concat([not_verbundprojekt, is_verbundprojekt], ignore_index=True)
        foerdersumme_subkategorien.append((df_subkatalog['Fördersumme in EUR'].sum() / 10e+5, str(i+1) + '.' + data_subkatalog[0] + ' ' + df_subkatalog.iloc[0]['vordefinierte Katalog']))
        anzahl_subkategorien.append((is_and_not_verbundprojekt.__len__(), str(i+1) + '.' + data_subkatalog[0] + ' ' + df_subkatalog.iloc[0]['vordefinierte Katalog']))
    foerdersumme_subkategorien.sort(reverse=True)
    anzahl_subkategorien.sort(reverse=True)

    _, ax2 = plt.subplots(figsize=(15, 10))
    sns.barplot(y=[x[1] for x in foerdersumme_subkategorien], x=[x[0] for x in foerdersumme_subkategorien], ax=ax2)
    ax2.set_xlabel('Fördersumme in Mio. EUR', fontsize=15, weight='bold')
    ax2.set_title('Fördersumme der KI-Projekten aller Sub-Katalogen von ' + str(i+1) + '. ' + vordefinierte_katalog[i], fontsize=20, weight='bold')
    plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/Fördersumme_Sub-Katalog_von_oberkatalog_' + str(i+1) + '.svg',
                bbox_inches='tight', format='svg', transparent=True)

    _, ax3 = plt.subplots(figsize=(15, 10))
    sns.barplot(y=[x[1] for x in anzahl_subkategorien], x=[x[0] for x in anzahl_subkategorien], ax=ax3)
    ax3.set_xlabel('Anzahl', fontsize=15, weight='bold')
    ax3.set_title('Anzahl der KI-Projekten aller Sub-Katalogen von ' + str(i + 1) + '. ' + vordefinierte_katalog[i], fontsize=20,
                  weight='bold')
    plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/Anzahl_Sub-Katalog_von_oberkatalog_' + str(
        i + 1) + '.svg',
                bbox_inches='tight', format='svg', transparent=True)
