#!/usr/bin/env python
# coding: utf-8
from datetime import date
from multiprocessing import Pool, Manager, cpu_count

import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database0 = 'data_AcaTech'
database1 = 'data_förderprogramm'
user = 'root'
host = 'localhost'
password = '1234'

# Verbindung zum Datenbank herstellen
db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database0)
sql1 = """SELECT Katalog FROM Table_Vordefinierte_Katalog"""
cur.execute(sql1)
vordefinierte_katalog = [x[0] for x in cur.fetchall()]
vordefinierte_katalog_klartext = ''
for i, begriff in enumerate(vordefinierte_katalog):
    if i == vordefinierte_katalog.__len__()-1:
        vordefinierte_katalog_klartext += begriff
        break
    vordefinierte_katalog_klartext += begriff + '|'

sql = """SELECT distinct übergeordnete_1, übergeordnete_1_Klartext FROM Table_Katalog;"""
cur.execute(sql)
oberkatalog = cur.fetchall()
anzahl_oberkatalog = oberkatalog.__len__()


dflist_KI = []
for i, katalog in enumerate(oberkatalog):
    cur.execute("USE " + database0)
    sql = """SELECT Katalog FROM Table_Katalog WHERE übergeordnete_1=""" + str(i+1)
    cur.execute(sql)
    ober_katalog = [x[0] for x in cur.fetchall()]
    ober_katalog_klartext = ''
    for i, begriff in enumerate(ober_katalog):
        if i == ober_katalog.__len__() - 1:
            ober_katalog_klartext += begriff
            break
        ober_katalog_klartext += begriff + '|'
    cur.execute("USE " + database1)
    sql = """SELECT A.title FROM (SELECT * FROM Table_Förderprogramm_details WHERE title REGEXP '""" + vordefinierte_katalog_klartext +"""') A WHERE A.title REGEXP '"""+ ober_katalog_klartext +"""';"""
    cur.execute(sql)
    results = cur.fetchall()
    _temp = pd.DataFrame(results, columns=["Thema"])
    _temp['katalog'] = katalog[0] + '. ' + katalog[1]
    dflist_KI.append(_temp)

df = pd.concat(dflist_KI)
df_new = df.groupby(['katalog'], as_index=False)['katalog'].agg({'cnt': 'count'})
fig1, ax1 = plt.subplots(figsize=(15, 10))
sns.barplot(y='katalog', x='cnt', data=df_new, order=df_new.sort_values('cnt', ascending=False).katalog, ax=ax1)
ax1.set_xlabel('Anzahl', fontsize=15, weight='bold')
ax1.set_title('Anzahl der KI-Bekanntmachungen von jeweiligem Ober-katalog', fontsize=20, weight='bold')
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/Gesamt_anzahl_KI-Bekanntmachungen_Ober-Katalog.svg',
                bbox_inches='tight', format='svg', transparent=True)


for i, katalog in enumerate(oberkatalog):
    sub_katalog_list = []
    sql = """SELECT distinct übergeordnete_2, übergeordnete_2_KlarText FROM data_AcaTech.Table_Katalog WHERE übergeordnete_1=""" + str(i+1)
    cur.execute(sql)
    results = cur.fetchall()
    sub_katalog_list.append(results)
    df_draw = pd.DataFrame(columns=['title', 'katalog'])
    for subkatalog in sub_katalog_list[0]:
        sql1 = """SELECT Katalog FROM data_AcaTech.Table_Katalog WHERE übergeordnete_2_KlarText= '""" + subkatalog[1] + """'"""
        cur.execute(sql1)
        sub_sub_katalog = [x[0] for x in cur.fetchall()]
        sub_sub_katalog_klartext = ''
        for w, begriff in enumerate(sub_sub_katalog):
            if w == sub_sub_katalog.__len__() - 1:
                sub_sub_katalog_klartext += begriff
                break
            sub_sub_katalog_klartext += begriff + '|'
        sql2 = """SELECT A.title FROM (SELECT * FROM data_förderprogramm.Table_Förderprogramm_details WHERE title REGEXP '""" + vordefinierte_katalog_klartext +"""') A WHERE A.title REGEXP '"""+ sub_sub_katalog_klartext +"""';"""
        cur.execute(sql2)
        results = cur.fetchall()
        _temp = pd.DataFrame(results, columns=['title'])
        _temp['katalog'] = str(i+1) + '.' + subkatalog[0] + ' ' + subkatalog[1]
        df_draw = pd.concat([df_draw, _temp], ignore_index=True)
    df_draw_new = df_draw.groupby(['katalog'], as_index=False)['katalog'].agg({'cnt': 'count'})
    fig2, ax2 = plt.subplots(figsize=(21, 10))
    sns.barplot(y='katalog', x='cnt', data=df_draw_new, order=df_draw_new.sort_values('cnt', ascending=False).katalog, ax=ax2)
    ax2.set_xlabel('Anzahl', fontsize=15, weight='bold')
    ax2.set(ylabel=None)
    ax2.set_title('Anzahl der KI-Bekanntmachungen von ' + katalog[0] + ' ' + katalog[1], fontsize=20, weight='bold')
    plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/anzahl_KI-Bekanntmachungen_Sub-Katalog_von_oberkatalog_'+ katalog[0] +'.svg',
                   bbox_inches='tight', format='svg', transparent=True)


