#!/usr/bin/env python
# coding: utf-8
import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database = 'data_AcaTech'
user = 'root'
host = 'localhost'
password = '1234'

db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database)
deadlinelimit = 2020

# select die Projekten in der Vorfilterungsklasse und Verbundprojekt und Uni/Institut als Zuwendungsempfaenger
sql = """SELECT A.id, A.`Fördersumme in EUR`, A.Thema, A.Zuwendungsempfänger, A.Bundesland_Zuwendungsempfänger, A.Ressort,
    A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM Table_Förderkatalog A WHERE A.Zuwendungsempfänger REGEXP 'Universität|Institut|Hochschule| TU |Fraunhofer' AND id IN (SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH) AND A.Verbundprojekt IS NOT NULL;"""
cur.execute(sql)
results = cur.fetchall()
# es wird hier nur teilweise exportiert aus Datenbank
df_KI_uni = pd.DataFrame(results, columns=["id", "Fördersumme in EUR", "Thema", "Zuwendungsempfänger",
                                       "Bundesland_Zuwendungsempfänger", "Ressort", "Laufzeit von",
                                       "Laufzeit bis", "Verbundprojekt"])

df_KI_uni = df_KI_uni[df_KI_uni["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1)]


sql = """SELECT A.id, A.`Fördersumme in EUR`, A.Thema, A.Zuwendungsempfänger, A.Bundesland_Zuwendungsempfänger, A.Ressort,
    A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM Table_Förderkatalog A WHERE NOT A.Zuwendungsempfänger REGEXP 'Universität|Institut|Hochschule| TU |Fraunhofer' AND id IN (SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH) AND A.Verbundprojekt IS NOT NULL;"""
cur.execute(sql)
results = cur.fetchall()
df_KI_unternehmen = pd.DataFrame(results, columns=["id", "Fördersumme in EUR", "Thema", "Zuwendungsempfänger",
                                       "Bundesland_Zuwendungsempfänger", "Ressort", "Laufzeit von",
                                       "Laufzeit bis", "Verbundprojekt"])

df_KI_unternehmen = df_KI_unternehmen[df_KI_unternehmen["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1)]


df_KI_uni["Zuwendungsempfänger"] = 'Universität'
df_KI_unternehmen["Zuwendungsempfänger"] = 'Unternehmen'


c = pd.concat([df_KI_uni, df_KI_unternehmen], ignore_index=True)
group = c.groupby("Verbundprojekt")
anteil_anzahl = pd.DataFrame(columns=['anteil', 'katalog'])
anteil_foerdersumme = pd.DataFrame(columns=['anteil', 'katalog'])
gesamt_summe_uni = 0
gesamt_summe_unternehmen = 0
for i, item in enumerate(group):
    a = item[1]
    if a.__len__() == 1:
        continue
    anzahl_uni = a[a["Zuwendungsempfänger"] == "Universität"].__len__()
    anzahl_unternehmen = a[a["Zuwendungsempfänger"] == "Unternehmen"].__len__()
    summe_uni = a[a["Zuwendungsempfänger"] == "Universität"]['Fördersumme in EUR'].sum()
    summe_unternehmen = a[a["Zuwendungsempfänger"] == "Unternehmen"]['Fördersumme in EUR'].sum()
    gesamt_summe_uni += summe_uni
    gesamt_summe_unternehmen += summe_unternehmen
    aaa = pd.DataFrame([[anzahl_uni / (anzahl_uni+anzahl_unternehmen), 'Forschungsinstitut']], columns=['anteil', 'katalog'])
    bbb = pd.DataFrame([[1-(anzahl_uni / (anzahl_uni+anzahl_unternehmen)), 'Unternehmen']], columns=['anteil', 'katalog'])

    ccc = pd.DataFrame([[summe_uni / (summe_uni+summe_unternehmen), 'Forschungsinstitut']], columns=['anteil', 'katalog'])
    ddd = pd.DataFrame([[1-(summe_uni / (summe_uni+summe_unternehmen)), 'Unternehmen']], columns=['anteil', 'katalog'])

    anteil_anzahl = pd.concat([aaa, bbb, anteil_anzahl], ignore_index=True)
    anteil_foerdersumme = pd.concat([ccc, ddd, anteil_foerdersumme], ignore_index=True)

gesamt_summe = pd.DataFrame([[gesamt_summe_uni/1e+6, 'Forschungsinstitut'],[gesamt_summe_unternehmen/1e+6, 'Unternehmen']], columns=['gesammt Summe in Mio. EUR', 'katalog'])

#sns.displot(anteil_anzahl[anteil_anzahl['katalog']=='Unternehmen'],x='anteil')
#sns.displot(anteil_anzahl[anteil_anzahl['katalog']=='Forschungsinstitut'],x='anteil')

fig1, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(20, 10))
sns.boxplot(y='anteil', x='katalog', data=anteil_anzahl, width=.6, ax=ax1)
sns.boxplot(y='anteil', x='katalog', data=anteil_foerdersumme, width=.6, ax=ax2)
sns.barplot(y='gesammt Summe in Mio. EUR', x='katalog', data=gesamt_summe, ax=ax3)
fig1.subplots_adjust(wspace=0.1, hspace=0)
ax1.set_title('(a) Anteil der Anzahl in der Koorperation')
ax2.set_title('(b) Anteil der Fördersumme in der Koorperation')
ax3.set_title('(c) Gesamte Fördersumme in der Koorperation')
fig1.subplots_adjust(wspace=0.3, hspace=0)
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/anteil_anzahl_fördersumme_Forschuninstitut_unternehmen.svg',
            bbox_inches='tight', format='svg', transparent=True)
