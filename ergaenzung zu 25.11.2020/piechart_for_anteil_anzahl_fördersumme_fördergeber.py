#!/usr/bin/env python
# coding: utf-8
from datetime import date
from multiprocessing import Pool, Manager, cpu_count

import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database = 'data_AcaTech'
user = 'root'
host = 'localhost'
password = '1234'

db = pymysql.connect(host, user, password)
cur = db.cursor()
cur.execute("USE " + database)
deadlinelimit = 2020

# select die Projekten in der Vorfilterungsklasse
sql = """SELECT A.id, A.`Fördersumme in EUR`, A.Thema, A.Bundesland_Zuwendungsempfänger, A.Ressort,
    A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM Table_Förderkatalog A WHERE id IN (SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH);"""
cur.execute(sql)
results = cur.fetchall()
# es wird hier nur teilweise exportiert aus Datenbank
df_KI = pd.DataFrame(results, columns=["id", "Fördersumme in EUR", "Thema",
                                       "Bundesland_Zuwendungsempfänger", "Ressort", "Laufzeit von",
                                       "Laufzeit bis", "Verbundprojekt"])

# select die Projekten nicht in der Vorfilterungsklasse
sql = """SELECT A.id, A.`Fördersumme in EUR`, A.Thema, A.Bundesland_Zuwendungsempfänger, A.Ressort,
    A.`Laufzeit von`, A.`Laufzeit bis`, A.Verbundprojekt FROM Table_Förderkatalog A WHERE id NOT IN (SELECT Table_Relation_VorD_TH.Förderkatalog_id FROM Table_Relation_VorD_TH);"""
cur.execute(sql)
results = cur.fetchall()
# es wird hier nur teilweise exportiert aus Datenbank
df_NOT_KI = pd.DataFrame(results, columns=["id", "Fördersumme in EUR", "Thema",
                                           "Bundesland_Zuwendungsempfänger", "Ressort", "Laufzeit von",
                                           "Laufzeit bis", "Verbundprojekt"])

df_KI = df_KI[df_KI["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1)]
df_NOT_KI = df_NOT_KI[df_NOT_KI["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1)]

class OP1:
    def __init__(self):
        self.manager = Manager
        self.mp_list = self.manager().list()

    def proc_func(self, partdataframe):
        # set_trace()
        if partdataframe[1].__len__() == 1:
            self.mp_list.append(partdataframe[1].to_dict('records')[0])
        else:
            _temp2 = pd.DataFrame([partdataframe[1].iloc[0]])
            _temp2['Fördersumme in EUR'] = partdataframe[1]["Fördersumme in EUR"].sum()
            self.mp_list.append(_temp2.to_dict('records')[0])

    def flow(self):
        pool = Pool(cpu_count())
        for i, partdataframe in enumerate(_temp1):
            pool.apply_async(self.proc_func, args=(partdataframe,))
        pool.close()
        pool.join()


df_KI1 = df_KI[df_KI['Verbundprojekt'].isnull()]
df_KI1.reset_index(inplace=True, drop=True)
# verbundprojekt
_temp = df_KI[~df_KI['Verbundprojekt'].isnull()]
_temp.reset_index(inplace=True, drop=True)
# klassifizieren der Verbundprojekten
_temp1 = _temp.groupby('Verbundprojekt')
op1 = OP1()
op1.flow()
lst1 = op1.mp_list
del _temp1, op1
df_temp = pd.DataFrame(lst1[:])
df_KI1 = pd.concat([df_KI1, df_temp], axis=0, ignore_index=True)
# q_low = df_KI1["Fördersumme in EUR"].quantile(0.01)
# q_hi = df_KI1["Fördersumme in EUR"].quantile(0.99)
# df_KI1 = df_KI1[(df_KI1["Fördersumme in EUR"] < q_hi) & (df_KI1["Fördersumme in EUR"] > q_low)]

df_NOT_KI1 = df_NOT_KI[df_NOT_KI['Verbundprojekt'].isnull()]
df_NOT_KI1.reset_index(inplace=True, drop=True)
# verbundprojekt
_temp = df_NOT_KI[~df_NOT_KI['Verbundprojekt'].isnull()]
_temp.reset_index(inplace=True, drop=True)
# klassifizieren der Verbundprojekten
_temp1 = _temp.groupby('Verbundprojekt')
op1 = OP1()
op1.flow()
lst1 = op1.mp_list
del _temp1, op1
df_temp = pd.DataFrame(lst1[:])
df_NOT_KI1 = pd.concat([df_NOT_KI1, df_temp], axis=0, ignore_index=True)
#q_low = df_NOT_KI1["Fördersumme in EUR"].quantile(0.01)
#q_hi = df_NOT_KI1["Fördersumme in EUR"].quantile(0.99)
#df_NOT_KI1 = df_NOT_KI1[(df_NOT_KI1["Fördersumme in EUR"] < q_hi) & (df_NOT_KI1["Fördersumme in EUR"] > q_low)]


# Projekten zählen und die Fördersumme berechnen
anzahl_KI_vs_not_KI = [df_KI1.__len__(), df_NOT_KI1.__len__()]
summe_KI_vs_not_KI = [df_KI1["Fördersumme in EUR"].sum(), df_NOT_KI1["Fördersumme in EUR"].sum()]
labels = ['KI Projekt', 'Kein KI Projekt']

# Aufteilung der Anzahl zeichnen
fig1, ax1 = plt.subplots(figsize=(10, 10))
ax1.pie(anzahl_KI_vs_not_KI, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=90)
ax1.set_title('Anteil der Anzahl von KI-Projekt', fontsize=20, weight='bold')
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/anteil_anzahl_KI-Projekt.svg',
            bbox_inches='tight', format='svg', transparent=True)

# Aufteilung der Fördersumme zeichnen
fig2, ax2 = plt.subplots(figsize=(10, 10))
ax2.pie(summe_KI_vs_not_KI, labels=labels, autopct='%1.1f%%',
        shadow=False, startangle=90)
ax2.set_title('Anteil der Fördersumme von KI-Projekt', fontsize=20, weight='bold')
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/anteil_fördersumme_KI-Projekt.svg',
            bbox_inches='tight', format='svg', transparent=True)

gesamt_foerdersumme = df_KI1['Fördersumme in EUR'].sum()
group = df_KI1.groupby('Ressort')
piedata = []
for i, partdataframe in enumerate(group):
    piedata.append([partdataframe[0], partdataframe[1]['Fördersumme in EUR'].sum()])

def takeSecond(elem):
    return elem[1]

piedata.sort(key=takeSecond, reverse=True)
piedata_mit_sonstiges = piedata[:3]
piedata_mit_sonstiges.append(['sontiges', sum(y[1] for y in piedata[3:])])

summe_foerdergeber = [d[1] for d in piedata_mit_sonstiges]
Ressort = [d[0] for d in piedata_mit_sonstiges]
fig3, ax3 = plt.subplots(figsize=(10, 10))
ax3.pie(summe_foerdergeber, labels=Ressort, autopct='%1.1f%%',
        shadow=False, startangle=90)
ax3.set_title('Verteilung des Fördergebers von KI-Projekt', fontsize=20, weight='bold')
ax3.axis('equal')
plt.savefig('/home/zf/GitHub/Acatech_daten/figure/new_25.11.2020/verteilung_fördergeber_KI-Projekt.svg',
            bbox_inches='tight', format='svg', transparent=True)
