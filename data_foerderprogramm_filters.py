#!/usr/bin/env python
# coding: utf-8

import pymysql
import requests
from bs4 import BeautifulSoup
from helpFunc import get_foerderprogram_hrefs
from multiprocessing import Pool, Manager, cpu_count

filters = {
    "foerderbereich":
        {
            "arbeit": {
                "url_extension": "&cl2Processes_Foerderbereich=arbeit",
                "sub_values":
                    {
                        "arbeitsgestaltung_und_arbeitsschutz": "&cl2Satisfaction_UFArbeit=arbeitsgestaltung_arbeitsschutz",
                        "ausserbetriebliche_arbeitsgelegenheiten": "&cl2Satisfaction_UFArbeit=ausserbetriebliche_arbeitsgelegenheiten",
                        "betriebliche_arbeitsplätze": "&cl2Satisfaction_UFArbeit=betriebliche_arbeitsplaetze",
                        "eingliederung_besonderer_zielgruppen": "&cl2Satisfaction_UFArbeit=eingliederung_besonderer_zielgruppen",
                        "qualifizierung_von_arbeitssuchenden": "&cl2Satisfaction_UFArbeit=qualifizierung_von_arbeitssuchenden",
                    },
            },
            "aus_weiterbildung": {
                "url_extension": "&cl2Processes_Foerderbereich=aus_weiterbildung",
                "sub_values":
                    {
                        "ausbildung_besonderer_zielgruppen": "&cl2Satisfaction_UFAusWeiterbildung=ausbildung_besonderer_zielgruppen",
                        "ausbildungsstrukturen_traeger": "&cl2Satisfaction_UFAusWeiterbildung=ausbildungsstrukturen_traeger",
                        "ausserbetriebliche_ausbildung": "&cl2Satisfaction_UFAusWeiterbildung=ausserbetriebliche_ausbildung",
                        "berufsvorbereitung": "&cl2Satisfaction_UFAusWeiterbildung=berufsvorbereitung",
                        "betriebliche_ausbildung": "&cl2Satisfaction_UFAusWeiterbildung=betriebliche_ausbildung",
                        "studienfoerderung": "&cl2Satisfaction_UFAusWeiterbildung=studienfoerderung",
                        "weiterbildung_unternehmer_selbststaendige": "&cl2Satisfaction_UFAusWeiterbildung=weiterbildung_unternehmer_selbststaendige",
                        "weiterbildung_von_arbeitnehmern": "&cl2Satisfaction_UFAusWeiterbildung=weiterbildung_von_arbeitnehmern",
                        "weiterbildung_von_arbeitssuchenden": "&cl2Satisfaction_UFAusWeiterbildung=weiterbildung_von_arbeitssuchenden",
                        "weiterbildungsstrukturen_traeger": "&cl2Satisfaction_UFAusWeiterbildung=weiterbildungsstrukturen_traeger",
                    },
            },
            "aussenwirtschaft": {
                "url_extension": "&cl2Processes_Foerderbereich=aussenwirtschaft",
                "sub_values":
                    {
                        "auslandsinvestitionen": "&cl2Satisfaction_UFAussenwirtschaft=auslandsinvestitionen",
                        "aussenwirtschaftsberatung": "&cl2Satisfaction_UFAussenwirtschaft=aussenwirtschaftsberatung",
                        "entwicklungszusammenarbeit": "&cl2Satisfaction_UFAussenwirtschaft=entwicklungszusammenarbeit",
                        "exportfinanzierung": "&cl2Satisfaction_UFAussenwirtschaft=exportfinanzierung",
                        "markterkundung_erschliessung": "&cl2Satisfaction_UFAussenwirtschaft=markterkundung_erschliessung",
                        "messen_ausstellungen": "&cl2Satisfaction_UFAussenwirtschaft=messen_ausstellungen",
                    },
            },
            "beratung": {
                "url_extension": "&cl2Processes_Foerderbereich=beratung",
                "sub_values":
                    {
                        "aus_weiterbildung": "&cl2Satisfaction_UFBeratung=aus_weiterbildung",
                        "aussenwirtschaft": "&cl2Satisfaction_UFBeratung=aussenwirtschaft",
                        "energieeffizienz_erneuerbare_energien": "&cl2Satisfaction_UFBeratung=energieeffizienz_erneuerbare_energien",
                        "existenzgruendung_festigung": "&cl2Satisfaction_UFBeratung=existenzgruendung_festigung",
                        "forschung_innovation": "&cl2Satisfaction_UFBeratung=forschung_innovation",
                        "umwelt_naturschutz": "&cl2Satisfaction_UFBeratung=umwelt_naturschutz",
                        "unternehmensfuehrung": "&cl2Satisfaction_UFBeratung=unternehmensfuehrung",
                    },
            },
            "corona_hilfe": {
                "url_extension": "&cl2Processes_Foerderbereich=corona",
                "sub_values": None,
            },
            "energieeffizienz_erneuerbare_energien": {
                "url_extension": "&cl2Processes_Foerderbereich=energieeffizienz_erneuerbare_energien",
                "sub_values": {
                    "beratung_schulung": "&cl2Satisfaction_UFEnergieeffizienz=beratung_schulung",
                    "betriebliche_private_investitionen": "&cl2Satisfaction_UFEnergieeffizienz=betriebliche_private_investitionen",
                    "forschung_innovation": "&cl2Satisfaction_UFEnergieeffizienz=forschung_innovation",
                    "oeffentliche_infrastruktur": "&cl2Satisfaction_UFEnergieeffizienz=oeffentliche_infrastruktur",
                    "wohnungsbau_modernisierung": "&cl2Satisfaction_UFEnergieeffizienz=wohnungsbau_modernisierung",
                },
            },
            "existenzgruendung_festigung": {
                "url_extension": "&cl2Processes_Foerderbereich=existenzgruendung_festigung",
                "sub_values": {
                    "beratung_schulung": "&cl2Satisfaction_UFExistenzgruendung=beratung_schulung",
                    "betriebsmittel": "&cl2Satisfaction_UFExistenzgruendung=betriebsmittel",
                    "festigungsinvestitionen": "&cl2Satisfaction_UFExistenzgruendung=festigungsinvestitionen",
                    "gruendungsinvestitionen": "&cl2Satisfaction_UFExistenzgruendung=gruendungsinvestitionen",
                    "gruendungsstrukturen_netzwerke": "&cl2Satisfaction_UFExistenzgruendung=gruendungsstrukturen_netzwerke",
                    "lebensunterhalt_soziale_sicherung": "&cl2Satisfaction_UFExistenzgruendung=lebensunterhalt_soziale_sicherung",
                },
            },
            "forschung_innovation_themenoffen": {
                "url_extension": "&cl2Processes_Foerderbereich=forschung_innovation_themenoffen",
                "sub_values": {
                    "beratung_schulung": "&cl2Satisfaction_UFForschungOffen=beratung_schulung",
                    "betriebliche_innovationen": "&cl2Satisfaction_UFForschungOffen=betriebliche_innovationen",
                    "forschung_entwicklung": "&cl2Satisfaction_UFForschungOffen=forschung_entwicklung",
                    "gruendung_von_technologieunternehmen": "&cl2Satisfaction_UFForschungOffen=gruendung_von_technologieunternehmen",
                    "kooperationen_netzwerke": "&cl2Satisfaction_UFForschungOffen=kooperationen_netzwerke",
                    "patentierung_verwertung": "&cl2Satisfaction_UFForschungOffen=patentierung_verwertung",
                },
            },
            "forschung_innovation_themenspezifisch": {
                "url_extension": "&cl2Processes_Foerderbereich=forschung_innovation_themenspezifisch",
                "sub_values": {
                    "bauen_wohnen": "&cl2Satisfaction_UFForschungSpezifisch=bauen_wohnen",
                    "biowissenschaften": "&cl2Satisfaction_UFForschungSpezifisch=biowissenschaften",
                    "energieeffizienz_erneuerbare_energien": "&cl2Satisfaction_UFForschungSpezifisch=energieeffizienz_erneuerbare_energien",
                    "geowissenschaften": "&cl2Satisfaction_UFForschungSpezifisch=geowissenschaften",
                    "gesundheit_medizin": "&cl2Satisfaction_UFForschungSpezifisch=gesundheit_medizin",
                    "grundlagenforschung": "&cl2Satisfaction_UFForschungSpezifisch=grundlagenforschung",
                    "information_kommunikation": "&cl2Satisfaction_UFForschungSpezifisch=information_kommunikation",
                    "land_forst_fischwirtschaft": "&cl2Satisfaction_UFForschungSpezifisch=land_forst_fischwirtschaft",
                    "luft_raumfahrt": "&cl2Satisfaction_UFForschungSpezifisch=luft_raumfahrt",
                    "mikrosysteme": "&cl2Satisfaction_UFForschungSpezifisch=mikrosysteme",
                    "mobilitaet_verkehr": "&cl2Satisfaction_UFForschungSpezifisch=mobilitaet_verkehr",
                    "nanotechnologie": "&cl2Satisfaction_UFForschungSpezifisch=nanotechnologie",
                    "optische_technologien": "&cl2Satisfaction_UFForschungSpezifisch=optische_technologien",
                    "produktion_dienstleistungen": "&cl2Satisfaction_UFForschungSpezifisch=produktion_dienstleistungen",
                    "nachwachsende_rohstoffe": "&cl2Satisfaction_UFForschungSpezifisch=nachwachsende_rohstoffe",
                    "schifffahrt_meerestechnik": "&cl2Satisfaction_UFForschungSpezifisch=schifffahrt_meerestechnik",
                    "sicherheitsforschung": "&cl2Satisfaction_UFForschungSpezifisch=sicherheitsforschung",
                    "umwelt_naturschutz": "&cl2Satisfaction_UFForschungSpezifisch=umwelt_naturschutz",
                    "werkstoffe": "&cl2Satisfaction_UFForschungSpezifisch=werkstoffe",
                    "wirtschafts_sozial_geisteswissenschaften": "&cl2Satisfaction_UFForschungSpezifisch=wirtschafts_sozial_geisteswissenschaften",
                },
            },
            "gesundheit_soziales": {
                "url_extension": "&cl2Processes_Foerderbereich=gesundheit_soziales",
                "sub_values": {
                    "behinderte_menschen": "&cl2Satisfaction_UFGesundheitSoziales=behinderte_menschen",
                    "buergerschaftliches_engagement": "&cl2Satisfaction_UFGesundheitSoziales=buergerschaftliches_engagement",
                    "gesundheit": "&cl2Satisfaction_UFGesundheitSoziales=gesundheit",
                    "gleichstellung": "&cl2Satisfaction_UFGesundheitSoziales=gleichstellung",
                    "integration": "&cl2Satisfaction_UFGesundheitSoziales=integration",
                    "kinder_jugendliche_familien": "&cl2Satisfaction_UFGesundheitSoziales=kinder_jugendliche_familien",
                    "senioren_pflege": "&cl2Satisfaction_UFGesundheitSoziales=senioren_pflege",
                },
            },
            "infrastruktur": {
                "url_extension": "&cl2Processes_Foerderbereich=infrastruktur",
                "sub_values": {
                    "aus_weiterbildung": "&cl2Satisfaction_UFInfrastruktur=aus_weiterbildung",
                    "energie_umwelt": "&cl2Satisfaction_UFInfrastruktur=energie_umwelt",
                    "forschung_innovation": "&cl2Satisfaction_UFInfrastruktur=forschung_innovation",
                    "gesundheit_soziales": "&cl2Satisfaction_UFInfrastruktur=gesundheit_soziales",
                    "information_kommunikation": "&cl2Satisfaction_UFInfrastruktur=information_kommunikation",
                    "kultur": "&cl2Satisfaction_UFInfrastruktur=kultur",
                    "laendliche_entwicklung": "&cl2Satisfaction_UFInfrastruktur=laendliche_entwicklung",
                    "staedtebau_stadterneuerung": "&cl2Satisfaction_UFInfrastruktur=staedtebau_stadterneuerung",
                    "tourismus": "&cl2Satisfaction_UFInfrastruktur=tourismus",
                    "verkehr": "&cl2Satisfaction_UFInfrastruktur=verkehr",
                },
            },
            "kultur_medien_sport": {
                "url_extension": "&cl2Processes_Foerderbereich=kultur_medien_sport",
                "sub_values": {
                    "denkmalschutz_pflege": "&cl2Satisfaction_UFKulturMedienSport=denkmalschutz_pflege",
                    "film_fernsehen": "&cl2Satisfaction_UFKulturMedienSport=film_fernsehen",
                    "kunst_kultur": "&cl2Satisfaction_UFKulturMedienSport=kunst_kultur",
                    "politische_bildung": "&cl2Satisfaction_UFKulturMedienSport=politische_bildung",
                    "sport_sportstaetten": "&cl2Satisfaction_UFKulturMedienSport=sport_sportstaetten",
                },
            },
            "landwirtschaft_laendliche_entwicklung": {
                "url_extension": "&cl2Processes_Foerderbereich=landwirtschaft_laendliche_entwicklung",
                "sub_values": {
                    "besondere_formen_der_landbewirtschaftung": "&cl2Satisfaction_UFLandwirtschaft=besondere_formen_der_landbewirtschaftung",
                    "einzelbetriebliche_foerderung": "&cl2Satisfaction_UFLandwirtschaft=einzelbetriebliche_foerderung",
                    "fischwirtschaft": "&cl2Satisfaction_UFLandwirtschaft=fischwirtschaft",
                    "forschung_innovation": "&cl2Satisfaction_UFLandwirtschaft=forschung_innovation",
                    "forstwirtschaft": "&cl2Satisfaction_UFLandwirtschaft=forstwirtschaft",
                    "laendliche_entwicklung": "&cl2Satisfaction_UFLandwirtschaft=laendliche_entwicklung",
                    "marktstrukturverbesserung": "&cl2Satisfaction_UFLandwirtschaft=marktstrukturverbesserung",
                    "wasserwirtschaft_kuestenschutz": "&cl2Satisfaction_UFLandwirtschaft=wasserwirtschaft_kuestenschutz",
                },
            },
            "messen_ausstellungen": {
                "url_extension": "&cl2Processes_Foerderbereich=messen_ausstellungen",
                "sub_values": {
                    "auslandsmessen": "&cl2Satisfaction_UFMessenAusstellungen=auslandsmessen",
                    "inlandsmessen": "&cl2Satisfaction_UFMessenAusstellungen=inlandsmessen",
                },
            },
            "regionalfoerderung": {
                "url_extension": "&cl2Processes_Foerderbereich=regionalfoerderung",
                "sub_values": {
                    "gewerbliche_wirtschaft": "&cl2Satisfaction_UFRegionalfoerderung=gewerbliche_wirtschaft",
                    "laendliche_entwicklung": "&cl2Satisfaction_UFRegionalfoerderung=laendliche_entwicklung",
                    "oeffentliche_infrastruktur": "&cl2Satisfaction_UFRegionalfoerderung=oeffentliche_infrastruktur",
                },
            },
            "smart_cities_regionen": {
                "url_extension": "&cl2Processes_Foerderbereich=smart_cities_regionen",
                "sub_values": None,
            },
            "staedtebau_stadterneuerung": {
                "url_extension": "&cl2Processes_Foerderbereich=staedtebau_stadterneuerung",
                "sub_values": {
                    "aktive_stadt_und_ortsteilzentren": "&cl2Satisfaction_UFStaedtebauStadterneuerung=aktive_stadt_und_ortsteilzentren",
                    "energetische_sanierung": "&cl2Satisfaction_UFStaedtebauStadterneuerung=energetische_sanierung",
                    "sanierung_entwicklung": "&cl2Satisfaction_UFStaedtebauStadterneuerung=sanierung_entwicklung",
                    "soziale_stadt": "&cl2Satisfaction_UFStaedtebauStadterneuerung=soziale_stadt",
                    "stadtumbau": "&cl2Satisfaction_UFStaedtebauStadterneuerung=stadtumbau",
                    "staedtebaulicher_denkmalschutz": "&cl2Satisfaction_UFStaedtebauStadterneuerung=staedtebaulicher_denkmalschutz",
                },
            },
            "umwelt_naturschutz": {
                "url_extension": "&cl2Processes_Foerderbereich=umwelt_naturschutz",
                "sub_values": {
                    "beratung_schulung": "&cl2Satisfaction_UFUmweltNaturschutz=beratung_schulung",
                    "betriebliche_private_investitionen": "&cl2Satisfaction_UFUmweltNaturschutz=betriebliche_private_investitionen",
                    "forschung_innovation": "&cl2Satisfaction_UFUmweltNaturschutz=forschung_innovation",
                    "naturschutz_landschaftspflege": "&cl2Satisfaction_UFUmweltNaturschutz=naturschutz_landschaftspflege",
                    "oeffentliche_infrastruktur": "&cl2Satisfaction_UFUmweltNaturschutz=oeffentliche_infrastruktur",
                },
            },
            "unternehmensfinanzierung": {
                "url_extension": "&cl2Processes_Foerderbereich=unternehmensfinanzierung",
                "sub_values": {
                    "beratung_schulung": "&cl2Satisfaction_UFUnternehmensfinanzierung=beratung_schulung",
                    "betriebliche_investitionen": "&cl2Satisfaction_UFUnternehmensfinanzierung=betriebliche_investitionen",
                    "betriebsmittel": "&cl2Satisfaction_UFUnternehmensfinanzierung=betriebsmittel",
                    "konsolidierung_stabilisierung": "&cl2Satisfaction_UFUnternehmensfinanzierung=konsolidierung_stabilisierung",
                    "kooperationen_netzwerke": "&cl2Satisfaction_UFUnternehmensfinanzierung=kooperationen_netzwerke",
                    "nachfolge": "&cl2Satisfaction_UFUnternehmensfinanzierung=nachfolge",
                },
            },
            "wohnungsbau_modernisierung": {
                "url_extension": "&cl2Processes_Foerderbereich=wohnungsbau_modernisierung",
                "sub_values": {
                    "modernisierung_sanierung": "&cl2Satisfaction_UFWohnungsbau=modernisierung_sanierung",
                    "soziale_wohnraumfoerderung": "&cl2Satisfaction_UFWohnungsbau=soziale_wohnraumfoerderung",
                    "wohnungsbau_erwerb": "&cl2Satisfaction_UFWohnungsbau=wohnungsbau_erwerb",
                },
            },
        },
    "foerdergebiet":
        {
            "bundesweit": "&cl2Processes_Foerdergebiet=_bundesweit",
            "baden-wuerttemburg": "&cl2Processes_Foerdergebiet=baden_wuerttemberg",
            "bayern": "&cl2Processes_Foerdergebiet=bayern",
            "berlin": "&cl2Processes_Foerdergebiet=berlin",
            "brandenburg": "&cl2Processes_Foerdergebiet=brandenburg",
            "bremen": "&cl2Processes_Foerdergebiet=bremen",
            "niedersachsen": "&cl2Processes_Foerdergebiet=de_ni",
            "hamburg": "&cl2Processes_Foerdergebiet=hamburg",
            "hessen": "&cl2Processes_Foerdergebiet=hessen",
            "mecklenburg-vorpommern": "&cl2Processes_Foerdergebiet=mecklenburg_vorpommern",
            "nordrhein-westfalen": "&cl2Processes_Foerdergebiet=nordrhein_westfalen",
            "rheinland-pfalz": "&cl2Processes_Foerdergebiet=rheinland_pfalz",
            "saarland": "&cl2Processes_Foerdergebiet=saarland",
            "sachsen": "&cl2Processes_Foerdergebiet=sachsen",
            "sachsen-anhalt": "&cl2Processes_Foerdergebiet=de_st",
            "schleswig-holstein": "&cl2Processes_Foerdergebiet=schleswig_holstein",
            "sonstige": "&cl2Processes_Foerdergebiet=sonstige",
            "thueringen": "&cl2Processes_Foerdergebiet=thueringen",
        },
    "grw_foerderung":
        {
            "grw_foerderung": "&cl2Processes_GRW=grw_foerderung",
            "keine_grw_foerderung": "&cl2Processes_GRW=keine_grw_foerderung",
        },
    "unternehmensgroesse":
        {
            "grosses_unternehmen": "&cl2Processes_Unternehmensgroesse=grosses_unternehmen",
            "kleines_unternehmen": "&cl2Processes_Unternehmensgroesse=kleines_unternehmen",
            "kleinstunternehmen": "&cl2Processes_Unternehmensgroesse=kleinstunternehmen",
            "mittleres_unternehmen": "&cl2Processes_Unternehmensgroesse=mittleres_unternehmen",
        },
}
base_URL = "https://www.foerderdatenbank.de/"
page_1_url = "https://www.foerderdatenbank.de/SiteGlobals/FDB/Forms/Suche/Expertensuche_Formular.html?resourceId=c4b4dbf3-4c29-4e70-9465-1f1783a8f117&input_=bd101467-e52a-4850-931d-5e2a691629e5&pageLocale=de&filterCategories=FundingProgram&filterCategories.GROUP=1&templateQueryString=&submit=Suchen"
page_url_start = "https://www.foerderdatenbank.de/SiteGlobals/FDB/Forms/Suche/Expertensuche_Formular.html?input_=bd101467-e52a-4850-931d-5e2a691629e5&gtp=%2526816beae2-d57e-4bdc-b55d-392bc1e17027_list%253D"
page_url_end = "&filterCategories.GROUP=1&submit=Suchen&resourceId=c4b4dbf3-4c29-4e70-9465-1f1783a8f117&filterCategories=FundingProgram&pageLocale=de"


database = "data_förderprogramm"
user = 'root'
host = 'localhost'
password = '1234'


class OP:
    def __init__(self, page_count, item):
        self.manager = Manager
        self.mp_list = self.manager().list()
        self.data = page_count
        self.a = item

    def proc_func(self, i):
        # set_trace()
        page = requests.get(page_url_start + str(i) + page_url_end + self.a['href'])
        soup = BeautifulSoup(page.content, 'html.parser')
        self.mp_list += get_foerderprogram_hrefs(soup)
        # except KeyboardInterrupt:
        #    sys.exit()

    def flow(self):
        pool = Pool(cpu_count())
        for i in range(2, self.data + 1):
            pool.apply_async(self.proc_func, args=(i,))
        pool.close()
        pool.join()


if __name__ == '__main__':
    db = pymysql.connect(host, user, password)
    cur = db.cursor(pymysql.cursors.DictCursor)
    cur.execute("USE " + database)

    # hauptindex (index1)
    sql = """CREATE TABLE table_filter_index1 (
        name    TEXT NOT NULL
    );"""
    sql1 = """ALTER TABLE table_filter_index1 ADD id INT NOT NULL PRIMARY KEY AUTO_INCREMENT;"""
    sql2 = """ALTER TABLE table_filter_index1 ADD UNIQUE KEY(name(20));"""
    try:
        cur.execute(sql)
        cur.execute(sql1)
        cur.execute(sql2)
    except:
        pass
    hauptindex = [(x[0],) for x in filters.items()]
    cur.executemany("""INSERT IGNORE INTO table_filter_index1(name) VALUES(%s)""", hauptindex)
    db.commit()


    sql = """CREATE TABLE table_filters_foerderdatenbank (
                         name                   TEXT       NOT NULL,
                         index1                 int        NOT NULL,
                         index2                 int        NOT NULL,
                         index3                 int            NULL,
                         href                   VARCHAR(255)           NULL
                         );"""
    sql1 = """ALTER TABLE table_filters_foerderdatenbank ADD id INT NOT NULL PRIMARY KEY AUTO_INCREMENT;"""  # add primary key
    sql2 = """ALTER TABLE table_filters_foerderdatenbank ADD UNIQUE KEY(name(200), href);"""  # unique constrain
    sql3 = """ALTER TABLE table_filters_foerderdatenbank ADD CONSTRAINT fk_filter_hauptid FOREIGN KEY (
                index1) REFERENCES table_filter_index1 (id);"""
    try:
        cur.execute(sql)
        cur.execute(sql1)
        cur.execute(sql2)
        cur.execute(sql3)
        db.commit()
    except:
        pass

    data = []
    for i, item in enumerate(filters.items()):
        for j, item_unter in enumerate(item[1].items()):
            try:
                if 'url_extension' in item_unter[1]:
                    data.append((item_unter[0], i + 1, j + 1, 0, item_unter[1]['url_extension']))
                for w, item_unter_unter in enumerate(item_unter[1]['sub_values'].items()):
                    data.append((item_unter_unter[0], i+1, j+1, w+1, item_unter_unter[1]))
                    assert type(item_unter_unter[0]) == str
                    print(str(i+1)+'.'+str(j+1)+'.'+str(w+1) + ' ' + item_unter_unter[0])
            except:
                if type(item_unter[1]) == dict:
                    data.append((item_unter[0], i + 1, j + 1, None, item_unter[1]['url_extension']))
                    print(str(i + 1) + '.' + str(j + 1) + '.' + str(0) + ' ' + item_unter[0])
                    continue
                data.append((item_unter[0], i + 1, j + 1, None, item_unter[1]))
                print(str(i + 1) + '.' + str(j + 1) + '.' + str(0) + ' ' + item_unter[0])

    try:
        cur.execute(sql)
    except:
        pass
    cur.executemany("""INSERT IGNORE INTO table_filters_foerderdatenbank (name, index1, index2, index3, href) VALUES(%s,%s,%s,%s,%s)""", data)
    db.commit()


    sql1 = """CREATE TABLE table_foerderdatenbank (
                         href                   VARCHAR(255)        NOT NULL,
                         filter_id               int        NOT NULL
                         );"""
    sql2 = """ALTER TABLE table_foerderdatenbank ADD id INT NOT NULL PRIMARY KEY AUTO_INCREMENT;"""  # add primary key
    sql3 = """ALTER TABLE table_foerderdatenbank ADD CONSTRAINT fk_filter_id FOREIGN KEY (
                filter_id) REFERENCES table_filters_foerderdatenbank (id); """
    sql4 = """ALTER TABLE table_foerderdatenbank ADD UNIQUE KEY (href,filter_id);"""
    sql5 = """ALTER TABLE table_foerderdatenbank ADD CONSTRAINT fk_filter_thema FOREIGN KEY (
                href) REFERENCES Table_Förderprogramm_details (href);"""
    try:
        cur.execute(sql1)
        cur.execute(sql2)
        cur.execute(sql3)
        cur.execute(sql4)
        cur.execute(sql5)
    except:
        pass
    db.commit()


    cur.execute("""SELECT * FROM table_filters_foerderdatenbank;""")
    data = cur.fetchall()

    for i, item in enumerate(data):
        program_refs = []
        # Get search landing page
        page = requests.get(page_1_url + item['href'])
        soup = BeautifulSoup(page.content, 'html.parser')
        # Get program refs from first page
        program_refs.extend(get_foerderprogram_hrefs(soup))
        # Get page count
        pagination = soup.find("div", class_="pagination")
        if pagination is not None:
            page_count = int(pagination.findAll("li")[-2].text)
            op = OP(page_count, item)
            op.flow()
            lst = op.mp_list
            program_refs = lst[:] + program_refs
        thema_list = [(x, item['id']) for x in program_refs]
        cur.executemany(
            """INSERT IGNORE INTO table_foerderdatenbank(href, filter_id) VALUES(%s,%s)""",
            thema_list)
        print(item['name'], ' commited', str(i))

    db.commit()
    cur.close()
    db.close()



