#!/usr/bin/env python
# coding: utf-8
from datetime import date
from multiprocessing import Pool, Manager, cpu_count

import pymysql

from visualisierung import *

sns.set()
# Benutzer Info für Datenbank
database = 'data_AcaTech'
user = 'root'
host = 'localhost'
password = '1234'

# Bilderordner erstellen, falls nicht vorhanden
if not os.path.exists('figure'):
    os.mkdir('figure')
if not os.path.exists('figure/wordcloud'):
    os.mkdir('figure/wordcloud')
if not os.path.exists('figure/Ressort_Katalog'):
    os.mkdir('figure/Ressort_Katalog')
if not os.path.exists('figure/heatingmap'):
    os.mkdir('figure/heatingmap')


# class für Multiprocessing Pipline
class OP_wordcloud:
    def __init__(self, datalist):
        self.manager = Manager
        self.datalist = datalist

    def proc_func(self, item):
        wordcloudProgramm(item, item.iloc[0]["vordefinierte Katalog"], DataframeList=False)

    def flow(self):
        pool = Pool(cpu_count())
        for item in self.datalist:
            pool.apply_async(self.proc_func, args=(item,))
        pool.close()
        pool.join()


#####################################################################################################

if __name__ == '__main__':
    # Verbindung zum Datenbank herstellen
    db = pymysql.connect(host, user, password)
    cur = db.cursor()
    cur.execute("USE " + database)

    sql1 = """SELECT distinct übergeordnete_2_KlarText FROM Table_Katalog"""
    cur.execute(sql1)
    vordefinierte_katalog = [x[0] for x in cur.fetchall()]

    datalist = []
    for i, katalog in enumerate(vordefinierte_katalog):
        sql = """SELECT T.id, T.Thema, K.Übergeordnete_1, K.Übergeordnete_2, K.übergeordnete_1_KlarText, 
        K.übergeordnete_2_KlarText, K.Katalog, T.`Fördersumme in EUR`, T.`Laufzeit von`, T.`Laufzeit bis`, 
        T.Bundesland_Zuwendungsempfänger, T.Ressort, T.Verbundprojekt FROM Table_Förderkatalog T, 
        Table_Relation_KA_TH R, Table_Katalog K WHERE T.id=R.Förderkatalog_id AND R.Katalog_id=K.id AND 
        K.übergeordnete_2_KlarText='""" + katalog + """'; """
        cur.execute(sql)
        results = cur.fetchall()

        # es wird hier nur teilweise Spalten exportiert aus Datenbank
        results = pd.DataFrame(results, columns=["id", "Thema", "index1", "index2", "klartext index1", "vordefinierte Katalog",
                                           "unter katalog", "Fördersumme in EUR", "Laufzeit von", "Laufzeit bis",
                                           "Bundesland_Zuwendungsempfänger", "Ressort", "Verbundprojekt"])
        results.drop_duplicates(subset=["id"], inplace=True)
        # Verbundprojekten vereinigen
        df_KI1 = results[results['Verbundprojekt'].isnull()]
        df_KI1.reset_index(inplace=True, drop=True)
        _temp = results[~results['Verbundprojekt'].isnull()]
        _temp.reset_index(inplace=True, drop=True)
        # klassifizieren der Verbundprojekten
        _temp1 = _temp.groupby('Verbundprojekt')
        for j, partdataframe in enumerate(_temp1):
            if partdataframe[1].__len__() == 1:
                df_KI1 = pd.concat([df_KI1, partdataframe[1]])
            else:
                _temp2 = pd.DataFrame([partdataframe[1].iloc[0]])
                _temp2['Fördersumme in EUR'] = partdataframe[1]["Fördersumme in EUR"].sum()
                df_KI1 = pd.concat([df_KI1, _temp2])

        # df_KI1 = df_KI1.drop_duplicates(subset=['Thema', 'Bundesland_Zuwendungsempfänger'], keep='first')
        q_low = df_KI1["Fördersumme in EUR"].quantile(0.01)
        q_hi = df_KI1["Fördersumme in EUR"].quantile(0.99)
        df_KI1 = df_KI1[(df_KI1["Fördersumme in EUR"] < q_hi) & (df_KI1["Fördersumme in EUR"] > q_low)]
        df_KI1['Fördersumme in Mio. EUR'] = df_KI1["Fördersumme in EUR"]/10e+5

        datalist.append(df_KI1)

        # index1 und index2 mit Katalogname vereinigen
    for item in datalist:
        _temp = item['index1'].str.cat(item['index2'], sep='.')
        item.loc[:, "vordefinierte Katalog"] = _temp.str.cat(item["vordefinierte Katalog"], sep=' ')

    # bescränken auf Laufzeit bis > 2020.1.1 Zeitfenster
    deadlinelimit = 2020
    datalist_new_zeitfilter = []
    for i, item in enumerate(datalist):
        datalist_new_zeitfilter.append(item[item["Laufzeit bis"] > datetime.date(deadlinelimit, 1, 1)])

    # wordcloud mit multiprocessing
    op = OP_wordcloud(datalist_new_zeitfilter)
    op.flow()
    del op

    # Boxplot für Fördersumme
    summeBoxplot(datalist_new_zeitfilter, name='fördersumme_detailkatalog')

    # anzahl jeweilige detail_katalog
    anzahlKatalog(datalist_new_zeitfilter, filter='vordefinierte Katalog',
                  name='anzahl_jeweiligen_detailkatalog')

    # Verteilung des Fördergebers
    pieChartRessort(datalist_new_zeitfilter, filter='Ressort', name='Ressort')

    # Zeitliche Verlauf der Anzahl von Förderprojekt
    zeitlicheVerlauf(datalist, name='zeitliche_Verlauf_detailkatalog',
                     start=date(1970, 1, 1), end=date(2020, 12, 31))

    # Heatmapping, beschreiben die Fördersumme des Förderprojekts jenach Bundesland
    heatingmap(datalist_new_zeitfilter)

    plt.close('all')
