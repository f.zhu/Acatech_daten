#!/usr/bin/env python
# coding: utf-8

import datetime
import os

import folium
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from PIL import Image
from wordcloud import WordCloud
from datetime import timedelta


# Andere Darstellung der Fördersumme für jeweilige vordefinierte Katalog
def summeBoxplot(datalist, name):
    df_new = pd.DataFrame(columns=datalist[0].columns)
    df_new2 = pd.DataFrame(columns=['katalog', 'Fördersumme in Mio. EUR'])
    for i, item in enumerate(datalist):
        df_new = pd.concat([df_new, item], axis=0, ignore_index=True)
        df_new2.loc[i, :] = [item.iloc[0]['vordefinierte Katalog'], item["Fördersumme in Mio. EUR"].sum()]

    fig1, (axes1, axes2) = plt.subplots(1, 2, figsize=(40, 15))
    fig2, axes3 = plt.subplots(figsize=(20, 30))
    sns.boxplot(x="Fördersumme in Mio. EUR", y="vordefinierte Katalog", data=df_new,
                whis=[0, 100], width=.6, palette="vlag", ax=axes1)
    # Add in points to show each observation
    sns.stripplot(x="Fördersumme in Mio. EUR", y="vordefinierte Katalog", data=df_new,
                  size=4, color=".3", linewidth=0, ax=axes1, alpha=0.5)
    axes1.set_xlabel('Fördersumme in Mio. EUR', fontsize=15, weight='bold')
    axes1.set_ylabel('Katalog', fontsize=15, weight='bold')
    axes1.set_title('Fördersummeverteilung vom jeweiligen Katalog', fontsize=20, weight='bold')

    sns.barplot(x='Fördersumme in Mio. EUR', y='katalog', palette="deep", ax=axes2, data=df_new2)
    axes2.set_xlabel('Fördersumme in Mio. EUR', fontsize=15, weight='bold')
    axes2.set_ylabel('')
    axes2.set_yticks([])
    axes2.set_title('Gesamt Fördersumme vom jeweiligen Katalog', fontsize=20, weight='bold')

    fig1.subplots_adjust(wspace=0.01, hspace=0)
    fig1.savefig('figure/' + name + '_boxplot.svg', bbox_inches='tight', format='svg', transparent=True)

    sns.violinplot(x="Fördersumme in Mio. EUR", y="vordefinierte Katalog", data=df_new,
                   whis=[0, 100], width=.6, palette="vlag", ax=axes3)
    axes3.set_xlabel('Fördersumme in Mio. EUR', fontsize=15, weight='bold')
    axes3.set_ylabel('Katalog', fontsize=15, weight='bold')
    axes3.set_title('Fördersummeverteilung vom jeweiligen Katalog', fontsize=20, weight='bold')
    fig2.savefig('figure/' + name + '_violinplot.svg', bbox_inches='tight', format='svg', transparent=True)


# anzahl der jeweiligen vordefinierten Katalog in Form von Barplot
def anzahlKatalog(datalist, filter, name):
    katalog_count = [(x.__len__(), x.iloc[0][filter]) for x in datalist]
    data = pd.DataFrame(katalog_count, columns=['count', 'name'])
    _, axes1 = plt.subplots(figsize=(20, 15))
    data = data.sort_values(by=['count'], ascending=False)
    sns.barplot(x='count', y='name', palette="deep", ax=axes1, data=data)
    plt.xlabel('Anzahl', fontsize=15, weight='bold')
    plt.ylabel('Katalog', fontsize=15, weight='bold')
    plt.xticks(size=10)
    axes1.set_title('Anzahl des Förderprojekts von jeweiligem ' + filter, fontsize=20, weight='bold')
    axes1.spines['top'].set_visible(False)
    axes1.spines['right'].set_visible(False)
    axes1.spines['bottom'].set_visible(False)
    axes1.spines['left'].set_visible(False)
    plt.savefig('figure/' + name + '.svg', bbox_inches='tight', format='svg', transparent=True)


# Wordcloud für jeweiligen Katalog
def wordcloudProgramm(data, name, DataframeList=True):
    a = name.split()
    a = [x.replace('_', ' ').strip() for x in a]
    filter = ['Teilprojekt', 'Teilvorhaben', 'Verbundprojekt', 'und', 'für', 'die', 'der', 'das', 'mit', 'von', 'eine',
              'einer', 'den', 'sowie', 'aus', 'eines', 'im', 'zur', 'Verbundvorhaben', 'de', 'bei', 'des', 'dem', 'in',
              'auf', 'am', 'Beispiel', 'ein', 'über', 'Erstellung', 'zu', 'durch', 'an', 'einem', 'Entwicklung',
              'Runde', 'Basis', 'LernenVerbundprojekt', '0Verbundprojekt', 'for', 'and', 'or', 'Künstliche','Intelligenz',
              'KI', 'Digital']
    #b = []
    #for begriff in vordefinierte_katalog:
    #    c = begriff.replace('_', ' ').strip()
    #    b += c.split()
    #filter += b
    for x in a:
        filter.append(x)
    text = ''
    if DataframeList:
        for item in data:
            for i in range(item.__len__()):
                text += item.iloc[i]['Thema']
    else:
        for i in range(data.__len__()):
            text += data.iloc[i]['Thema']
    text_file = open('_temp.txt', 'w')
    text_file.write(text)
    text_file.close()

    f = open(u'_temp.txt', 'r').read()
    zahnrad = np.array(Image.open('data/mask_wordcloud.png'))
    try:
        wordcloud = WordCloud(stopwords=filter, background_color="white", width=2560, height=1440,
                              mask=zahnrad).generate(f)
        wordcloud_svg = wordcloud.to_svg(embed_font=True)
    except Exception as err:
        print(err)
        return
    f = open('figure/wordcloud/' + name + '_wordcloud.svg', "w+")
    f.write(wordcloud_svg)
    f.close()
    #plt.imshow(wordcloud)
    #plt.axis("off")
    #plt.savefig('figure/wordcloud/' + name + '_wordcloud.svg', bbox_inches='tight', dpi=1000, format='svg', transparent=True)
    os.remove("_temp.txt")


# Zeitliche Verläufe der Förderprogramme für jeweilige vordefinierten Katalog in Form von linieplot
def zeitlicheVerlauf(datalist, name, start, end):
    def daterange(start_date, end_date):
        for n in range(0, int((end_date - start_date).days), 30):
            yield start_date + timedelta(n)

    line_data = pd.DataFrame(columns=['zeit', 'Anzahl', 'Katalog'])
    for single_date in daterange(start, end):
        for item in datalist:
            zahl = item[(item['Laufzeit von'] <= single_date) & (item['Laufzeit bis'] >= single_date)].__len__()
            # print(single_date.strftime("%Y-%m-%d"))
            # print(zahl)
            linedata = []
            linedata.append([single_date, zahl])
            linedata = tuple(linedata)
            linedata = pd.DataFrame(linedata, columns=['zeit', 'Anzahl'])
            linedata.loc[:, 'Katalog'] = item.iloc[0]['vordefinierte Katalog']
            line_data = pd.concat([line_data, linedata], axis=0, ignore_index=True)

    combined = line_data.apply(pd.to_numeric, errors='ignore')
    _, axes3 = plt.subplots(figsize=(15, 10))
    sns.lineplot(x='zeit', y='Anzahl', data=combined, hue='Katalog', ax=axes3, lw=2, palette="Spectral", alpha=0.7)
    plt.xlabel('Zeit', fontsize=15, weight='bold')
    plt.ylabel('Anzahl', fontsize=15, weight='bold')
    axes3.set_title('Zeitliche Entwicklung der Förderprojektanzahl', fontsize=20, weight='bold')
    plt.savefig('figure/' + name + '.svg', bbox_inches='tight', format='svg', transparent=True)


# Piechart des Ressort für jeweilige vordefinierten Katalog
def pieChartRessort(datalist, filter, name='', DataframeList=True):
    def pieChartDraw(item, filter, name, *args):
        group = item.groupby(filter)
        piedata = []
        for i, partdataframe in enumerate(group):
            piedata.append((partdataframe[0], partdataframe[1].__len__()))
        sizes = [d[1] for d in piedata]
        labels = [d[0] for d in piedata]
        fig1, ax1 = plt.subplots(figsize=(15, 10))
        ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
                shadow=False, startangle=90, labeldistance=1.05)
        ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
        if args:
            ax1.set_title(filter + ' Verteilung von ' + datalist[j].iloc[0]['vordefinierte Katalog'], fontsize=20, weight='bold')
            plt.savefig('figure/Ressort_Katalog/' + datalist[j].iloc[0]['vordefinierte Katalog'] + '_' + name + '.svg', format='svg', transparent=True)
        else:
            ax1.set_title(filter + ' Verteilung von allen KI-Projekten', fontsize=20, weight='bold')
            plt.savefig('figure/Ressort_Katalog/gesamt_' + name + '.svg',
                        format='svg', transparent=True)
        plt.close()

    if DataframeList:
        for j, item in enumerate(datalist):
            pieChartDraw(item, filter, name, j)
    else:
        df_new = pd.DataFrame(columns=datalist[0].columns)
        for item in datalist:
            df_new = pd.concat([df_new, item], axis=0, ignore_index=True)
        df_new = df_new.drop_duplicates(subset=[filter, 'title'])
        pieChartDraw(df_new, filter, name)


# Heatingmap nach Bundesland
def heatingmap(datalist, name='', bins=''):
    for item in datalist:
        data = item
        group = data.groupby("Bundesland_Zuwendungsempfänger")
        Bundesland_data = []
        for i, partdataframe in enumerate(group):
            Bundesland_data.append((partdataframe[0], partdataframe[1]['Fördersumme in Mio. EUR'].sum()))
        Bundesland_data = tuple(Bundesland_data)
        Bundesland_data = pd.DataFrame(Bundesland_data, columns=['Bundesland', 'Fördersumme'])
        Bundesland_data['Katalog'] = item.iloc[0]['vordefinierte Katalog']
        deu_geo = 'data/bundeslaender_simplify200.geojson'
        deu_map = folium.Map(location=(52, 10), zoom_start=6)
        folium.GeoJson(
            deu_geo,
            style_function=lambda feature: {
                'fillColor': '#ffff00',
                'color': 'black',
                'weight': 2,
                'dashArray': '5, 5'
            }
        ).add_to(deu_map)
        folium.Choropleth(
            geo_data=deu_geo,
            data=Bundesland_data,
            columns=['Bundesland', 'Fördersumme'],
            key_on='feature.properties.name',
            fill_color='YlOrRd',
            fill_opacity=0.7,
            line_opacity=0.2,
            highlight=True,
            legend_name='Fördersumme in Mio. EUR ' + item.iloc[0]['vordefinierte Katalog'] + ' in der Bundesländer'
        ).add_to(deu_map)
        # display map
        if name == '':
            deu_map.save('figure/heatingmap/' + item.iloc[0]['vordefinierte Katalog'] + '.html')
        else:
            deu_map.save('figure/heatingmap/' + name + '.html')

        # als PNG Sepichern (sehr langsam!!)
        # img_data = deu_map._to_png(5)
        # img = Image.open(io.BytesIO(img_data))
        # img.save('image.png')


