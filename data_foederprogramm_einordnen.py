#!/usr/bin/env python
# coding: utf-8

import sys
from multiprocessing import Pool, cpu_count, Manager

import pymysql
import requests
from bs4 import BeautifulSoup

from helpFunc import get_foerderprogram_hrefs


def databaseEstablish(cursor, database):
    sql = "CREATE DATABASE " + database  # set up the database
    #sql1 = "ALTER DATABASE " + database + " character set utf8"  # chinese and japanese support
    cursor.execute(sql)
    #cursor.execute(sql1)

# Url von der Seiten definieren
base_URL = "https://www.foerderdatenbank.de/"
page_1_url = "https://www.foerderdatenbank.de/SiteGlobals/FDB/Forms/Suche/Expertensuche_Formular.html?resourceId=c4b4dbf3-4c29-4e70-9465-1f1783a8f117&input_=bd101467-e52a-4850-931d-5e2a691629e5&pageLocale=de&filterCategories=FundingProgram&filterCategories.GROUP=1&templateQueryString=&submit=Suchen"
page_url_start = "https://www.foerderdatenbank.de/SiteGlobals/FDB/Forms/Suche/Expertensuche_Formular.html?input_=bd101467-e52a-4850-931d-5e2a691629e5&gtp=%2526816beae2-d57e-4bdc-b55d-392bc1e17027_list%253D"
page_url_end = "&filterCategories.GROUP=1&submit=Suchen&resourceId=c4b4dbf3-4c29-4e70-9465-1f1783a8f117&filterCategories=FundingProgram&pageLocale=de"

# Benutzer Info für Datenbank
database = "data_förderprogramm"
user = 'root'
host = 'localhost'
password = '1234'

class OP:
    def __init__(self, page_count):
        self.manager = Manager
        self.mp_list = self.manager().list()
        self.data = page_count

    def proc_func(self, i):
        # set_trace()
        print('Seite ' + str(i))
        page = requests.get(page_url_start + str(i) + page_url_end)
        soup = BeautifulSoup(page.content, 'html.parser')
        self.mp_list += [(x,) for x in get_foerderprogram_hrefs(soup)]

    def flow(self):
        pool = Pool(cpu_count())
        for i in range(2, self.data + 1):
            pool.apply_async(self.proc_func, args=(i,))
        pool.close()
        pool.join()




if __name__ == '__main__':
    # Verbindung zum Datenbank herstellen
    db = pymysql.connect(host, user, password)
    cur = db.cursor()
    cur.execute("show databases")
    databaselist = [x[0] for x in cur.fetchall()]
    if database in databaselist:
        cur.execute("drop schema " + database)
        db.commit()
    databaseEstablish(cur, database)
    cur.execute("USE " + database)


    # Tabellen erstellen
    try:
        cur.execute("""CREATE TABLE Table_Förderprogramm(Thema_Link_html TEXT NOT NULL);""")
        cur.execute(
            """ALTER TABLE Table_Förderprogramm ADD id INT NOT NULL PRIMARY KEY AUTO_INCREMENT;""")  # add primary key
        cur.execute("""ALTER TABLE Table_Förderprogramm ADD UNIQUE KEY(Thema_Link_html(200));""")  # unique constrain
    except Exception as err:
        print(err)

    # Änderung abgeben
    db.commit()

    all_program_refs = []
    # Get search landing page
    page = requests.get(page_1_url)
    soup = BeautifulSoup(page.content, 'html.parser')
    # Get program refs
    all_program_refs.extend([(x,) for x in get_foerderprogram_hrefs(soup)])
    # Get page count
    pagination = soup.find("div", class_="pagination")
    if pagination is not None:
        page_count = int(pagination.findAll("li")[-2].text)
    else:
        sys.exit()
    op = OP(page_count)
    op.flow()
    lst = op.mp_list
    program_refs = all_program_refs + lst[:]
    print(len(program_refs))
    try:
        cur.executemany(
            """INSERT IGNORE INTO Table_Förderprogramm(Thema_Link_html) VALUES(%s)""", program_refs)
        print('ok')
    except Exception as err:
        print(err)

    db.commit()
    cur.close()
    db.close()
